(* ****** ****** *)

#include "./../midterm-1.dats"

(* ****** ****** *)

typedef ints = list0(int)
typedef intss = list0(ints)

(* ****** ****** *)

implement
mylist_submaxord
  (xs) = let
//
fun
aux
(
  xs: ints
) : intss =
(
case+ xs of
| list0_nil() =>
  list0_sing(list0_nil)
| list0_cons
    (x, xs) => let
    val xss = aux(xs)
  in
    mylist_foldright_cloref<ints,intss>
    ( xss
    , lam(ys, res) =>
      (case+ ys of
       | nil0() =>
         cons0(list0_sing(x), res)
       | cons0(y, _) =>
         if x <= y then cons0(cons0(x, ys), res) else res
      )
    , list0_nil(*void*)
    ) + (xss)
  end // end of [let]
)
//
in
  mylist_foldleft_cloref<ints,ints>
  ( aux(xs)
  , list0_nil()
  , lam(res, ys) => if length(res) >= length(ys) then res else ys
  ) (* end of [mylist_foldleft_cloref] *)
end // end of [mylist_submaxord]

(* ****** ****** *)

implement
main0() = () where
{
//
val xs =
  g0ofg1($list{int}(1,3,2,2,4,5))
//
val () = println! ("submaxord(xs) = ", mylist_submaxord(xs))
//
} (* end of [main0] *)

(* ****** ****** *)

(* end of [mylist_submaxord.dats] *)
