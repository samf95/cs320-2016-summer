(* ****** ****** *)

#include
"share/atspre_staload.hats"
#include
"share/HATS/atspre_staload_libats_ML.hats"

(* ****** ****** *)

staload
"./../../../mylib/mylist.dats"
staload
"./../../../mylib/mystream.dats"

(* ****** ****** *)

#include "./../midterm-2.dats"

(* ****** ****** *)
//
extern
fun
{a:t@ype}
stream_interleave(stream(a), stream(a)): stream(a)
//
implement
{a}(*tmp*)
stream_interleave(xs, ys) = $delay
(
let
  val-stream_cons(x, xs) = !xs in stream_cons(x, stream_interleave(ys, xs))
end
) (* end of [stream_interleave] *)
//
(* ****** ****** *)

implement
{a,b}
stream_xprod
  (xs, ys) = $delay(
//
let
//
val-
stream_cons(x0, xs1) = !xs
//
val xys =
  mystream_map_cloref<b, $tup(a,b)>(ys, lam(y) => $tup(x0, y))
//
val xys2 = stream_xprod(xs1, ys)
//
in
  !(stream_interleave(xys, xys2))
end
) (* end of [stream_xprod] *)

(* ****** ****** *)
//
fun
from(n: int): stream(int) =
  $delay(stream_cons(n, from(n+1)))
//
(* ****** ****** *)

implement
main0((*void*)) =
{
//
  val xs = from(0)
  val ys = from(0)
//
  val xys =
    stream_xprod<int,int>(xs, ys)
  // end of [val]
//
  val-stream_cons(xy, xys) = !xys
  val () = println! ("xy = ", xy.0, ", ", xy.1)
  val-stream_cons(xy, xys) = !xys
  val () = println! ("xy = ", xy.0, ", ", xy.1)
  val-stream_cons(xy, xys) = !xys
  val () = println! ("xy = ", xy.0, ", ", xy.1)
  val-stream_cons(xy, xys) = !xys
  val () = println! ("xy = ", xy.0, ", ", xy.1)
  val-stream_cons(xy, xys) = !xys
  val () = println! ("xy = ", xy.0, ", ", xy.1)
  val-stream_cons(xy, xys) = !xys
  val () = println! ("xy = ", xy.0, ", ", xy.1)
  val-stream_cons(xy, xys) = !xys
  val () = println! ("xy = ", xy.0, ", ", xy.1)
  val-stream_cons(xy, xys) = !xys
  val () = println! ("xy = ", xy.0, ", ", xy.1)
  val-stream_cons(xy, xys) = !xys
  val () = println! ("xy = ", xy.0, ", ", xy.1)
  val-stream_cons(xy, xys) = !xys
  val () = println! ("xy = ", xy.0, ", ", xy.1)
//
} (* end of [main0] *)

(* ****** ****** *)

(* end of [Q7.dats] *)
