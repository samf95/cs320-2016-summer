(* ****** ****** *)

#include
"share/atspre_staload.hats"
#include
"share/HATS/atspre_staload_libats_ML.hats"

(* ****** ****** *)

staload
"./../../../mylib/mylist.dats"
staload
"./../../../mylib/mystream.dats"

(* ****** ****** *)

#include "./../midterm-2.dats"

(* ****** ****** *)

implement
{a}
stream_pair(xs) = let
//
fun
aux
(
  xs: stream(a), prfx: list0(a)
) : stream($tup(a,a)) = $delay
(
let
  val-stream_cons(x0, xs) = !xs
in
  aux2(xs, x0, prfx, list0_reverse(prfx))
end
)
//
and
aux2
(
  xs: stream(a), x0: a, prfx: list0(a), ys: list0(a)
) : stream_con($tup(a,a)) =
(
  case+ ys of
  | list0_nil() => !(aux(xs, list0_cons(x0, prfx)))
  | list0_cons(y, ys) => stream_cons($tup(y, x0), $delay(aux2(xs, x0, prfx, ys)))
)
//
in
  aux(xs, list0_nil())
end // end of [stream_pair]

(* ****** ****** *)
//
fun
from(n: int): stream(int) =
  $delay(stream_cons(n, from(n+1)))
//
(* ****** ****** *)

implement
main0((*void*)) =
{
  val xs = from(0)
  val xys = stream_pair<int>(xs)
  val-stream_cons(xy, xys) = !xys
  val () = println! ("xy = ", xy.0, ", ", xy.1)
  val-stream_cons(xy, xys) = !xys
  val () = println! ("xy = ", xy.0, ", ", xy.1)
  val-stream_cons(xy, xys) = !xys
  val () = println! ("xy = ", xy.0, ", ", xy.1)
  val-stream_cons(xy, xys) = !xys
  val () = println! ("xy = ", xy.0, ", ", xy.1)
  val-stream_cons(xy, xys) = !xys
  val () = println! ("xy = ", xy.0, ", ", xy.1)
  val-stream_cons(xy, xys) = !xys
  val () = println! ("xy = ", xy.0, ", ", xy.1)
  val-stream_cons(xy, xys) = !xys
  val () = println! ("xy = ", xy.0, ", ", xy.1)
  val-stream_cons(xy, xys) = !xys
  val () = println! ("xy = ", xy.0, ", ", xy.1)
  val-stream_cons(xy, xys) = !xys
  val () = println! ("xy = ", xy.0, ", ", xy.1)
  val-stream_cons(xy, xys) = !xys
  val () = println! ("xy = ", xy.0, ", ", xy.1)
}

(* ****** ****** *)

(* end of [Q6.dats] *)
