######
##
## The Python3 code
## is generated from ATS source by atscc2py3
## The starting compilation time is: 2016-6-28: 21h:22m
##
######

######
#ATSextcode_beg()
######
######
#
from ats2pypre_basics_cats import *
#
from ats2pypre_PYlist_cats import *
#
######
######
#ATSextcode_end()
######

def ats2pypre_ref(arg0):
  tmpret0 = None
  funlab_py = None
  tmplab_py = None
  #__patsflab_ref
  tmpret0 = ats2pypre_ref_make_elt(arg0)
  return tmpret0


def ats2pypre_ref_make_elt(arg0):
  tmpret1 = None
  tmp2 = None
  funlab_py = None
  tmplab_py = None
  #__patsflab_ref_make_elt
  tmp2 = PYlist_sing(arg0)
  tmpret1 = tmp2
  return tmpret1


def ats2pypre_ref_get_elt(arg0):
  tmpret3 = None
  funlab_py = None
  tmplab_py = None
  #__patsflab_ref_get_elt
  tmpret3 = PYlist_get_at(arg0, 0)
  return tmpret3


def ats2pypre_ref_set_elt(arg0, arg1):
  funlab_py = None
  tmplab_py = None
  #__patsflab_ref_set_elt
  PYlist_set_at(arg0, 0, arg1)
  return#_void


def ats2pypre_ref_exch_elt(arg0, arg1):
  tmpret5 = None
  tmp6 = None
  funlab_py = None
  tmplab_py = None
  #__patsflab_ref_exch_elt
  tmp6 = PYlist_get_at(arg0, 0)
  PYlist_set_at(arg0, 0, arg1)
  tmpret5 = tmp6
  return tmpret5

######
##
## end-of-compilation-unit
##
######
