######
##
## The Python3 code
## is generated from ATS source by atscc2py3
## The starting compilation time is: 2016-6-28: 21h:35m
##
######

######
#ATSextcode_beg()
######
######
from libatscc2py_all import *
######
from ats2pylibc_random_cats import *
######
from ats2py_pygame_pyame_cats import *
######
sys.setrecursionlimit(1000000)
######
######
#ATSextcode_end()
######

#ATSassume(_057_home_057_hwxi_057_Teaching_057_CS320_057_cs320_055_2016_055_summer_055_hwxi_057_assigns_057_6_057_Wormlike_057_MySolution_057_Wormlike_056_sats__worm)

Wormlike__statmp10 = None

Wormlike__statmp192 = None

Wormlike__statmp193 = None

Wormlike__statmp195 = None

Wormlike__statmp223 = None

Wormlike__statmp224 = None

Wormlike__statmp225 = None

Wormlike__statmp226 = None

Wormlike__statmp227 = None

def Wormlike__patsfun_41__closurerize(env0):
  def Wormlike__patsfun_41__cfun(cenv, arg0, arg1): return Wormlike__patsfun_41(cenv[1], arg0, arg1)
  return (Wormlike__patsfun_41__cfun, env0)

def Wormlike__patsfun_68__closurerize(env0):
  def Wormlike__patsfun_68__cfun(cenv, arg0, arg1): return Wormlike__patsfun_68(cenv[1], arg0, arg1)
  return (Wormlike__patsfun_68__cfun, env0)

def Wormlike__patsfun_69__closurerize(env0):
  def Wormlike__patsfun_69__cfun(cenv, arg0, arg1): return Wormlike__patsfun_69(cenv[1], arg0, arg1)
  return (Wormlike__patsfun_69__cfun, env0)

def Wormlike__patsfun_71__closurerize(env0):
  def Wormlike__patsfun_71__cfun(cenv, arg0, arg1): return Wormlike__patsfun_71(cenv[1], arg0, arg1)
  return (Wormlike__patsfun_71__cfun, env0)

def Wormlike__patsfun_76__closurerize(env0):
  def Wormlike__patsfun_76__cfun(cenv, arg0): return Wormlike__patsfun_76(cenv[1], arg0)
  return (Wormlike__patsfun_76__cfun, env0)

def Wormlike__patsfun_85__85__1__closurerize(env0):
  def Wormlike__patsfun_85__85__1__cfun(cenv, arg0, arg1): return Wormlike__patsfun_85__85__1(cenv[1], arg0, arg1)
  return (Wormlike__patsfun_85__85__1__cfun, env0)

def Wormlike__patsfun_92__closurerize(env0):
  def Wormlike__patsfun_92__cfun(cenv, arg0): return Wormlike__patsfun_92(cenv[1], arg0)
  return (Wormlike__patsfun_92__cfun, env0)

def Wormlike__patsfun_107__107__1__closurerize(env0):
  def Wormlike__patsfun_107__107__1__cfun(cenv, arg0, arg1): return Wormlike__patsfun_107__107__1(cenv[1], arg0, arg1)
  return (Wormlike__patsfun_107__107__1__cfun, env0)

def Wormlike__patsfun_96__96__1__closurerize():
  def Wormlike__patsfun_96__96__1__cfun(cenv, arg0): return Wormlike__patsfun_96__96__1(arg0)
  return (Wormlike__patsfun_96__96__1__cfun, )

def Wormlike__patsfun_97__97__1__closurerize():
  def Wormlike__patsfun_97__97__1__cfun(cenv, arg0): return Wormlike__patsfun_97__97__1(arg0)
  return (Wormlike__patsfun_97__97__1__cfun, )

def Wormlike__patsfun_98__98__1__closurerize(env0):
  def Wormlike__patsfun_98__98__1__cfun(cenv, arg0): return Wormlike__patsfun_98__98__1(cenv[1], arg0)
  return (Wormlike__patsfun_98__98__1__cfun, env0)

def Wormlike__patsfun_124__closurerize(env0):
  def Wormlike__patsfun_124__cfun(cenv): return Wormlike__patsfun_124(cenv[1])
  return (Wormlike__patsfun_124__cfun, env0)

def _057_home_057_hwxi_057_Teaching_057_CS320_057_cs320_055_2016_055_summer_055_hwxi_057_assigns_057_6_057_Wormlike_057_MySolution_057_Wormlike_056_sats__succ_row(arg0):
  tmpret0 = None
  tmp1 = None
  tmp2 = None
  funlab_py = None
  tmplab_py = None
  #__patsflab_succ_row
  tmp1 = ats2pypre_add_int0_int0(arg0, 1)
  tmp2 = ats2pypre_lt_int0_int0(tmp1, 32)
  if (tmp2):
    tmpret0 = tmp1
  else:
    tmpret0 = 0
  #endif
  return tmpret0


def _057_home_057_hwxi_057_Teaching_057_CS320_057_cs320_055_2016_055_summer_055_hwxi_057_assigns_057_6_057_Wormlike_057_MySolution_057_Wormlike_056_sats__pred_row(arg0):
  tmpret3 = None
  tmp4 = None
  funlab_py = None
  tmplab_py = None
  #__patsflab_pred_row
  tmp4 = ats2pypre_gt_int0_int0(arg0, 0)
  if (tmp4):
    tmpret3 = ats2pypre_sub_int0_int0(arg0, 1)
  else:
    tmpret3 = ats2pypre_sub_int1_int1(32, 1)
  #endif
  return tmpret3


def _057_home_057_hwxi_057_Teaching_057_CS320_057_cs320_055_2016_055_summer_055_hwxi_057_assigns_057_6_057_Wormlike_057_MySolution_057_Wormlike_056_sats__succ_col(arg0):
  tmpret5 = None
  tmp6 = None
  tmp7 = None
  funlab_py = None
  tmplab_py = None
  #__patsflab_succ_col
  tmp6 = ats2pypre_add_int0_int0(arg0, 1)
  tmp7 = ats2pypre_lt_int0_int0(tmp6, 32)
  if (tmp7):
    tmpret5 = tmp6
  else:
    tmpret5 = 0
  #endif
  return tmpret5


def _057_home_057_hwxi_057_Teaching_057_CS320_057_cs320_055_2016_055_summer_055_hwxi_057_assigns_057_6_057_Wormlike_057_MySolution_057_Wormlike_056_sats__pred_col(arg0):
  tmpret8 = None
  tmp9 = None
  funlab_py = None
  tmplab_py = None
  #__patsflab_pred_col
  tmp9 = ats2pypre_gt_int0_int0(arg0, 0)
  if (tmp9):
    tmpret8 = ats2pypre_sub_int0_int0(arg0, 1)
  else:
    tmpret8 = ats2pypre_sub_int1_int1(32, 1)
  #endif
  return tmpret8


def _057_home_057_hwxi_057_Teaching_057_CS320_057_cs320_055_2016_055_summer_055_hwxi_057_assigns_057_6_057_Wormlike_057_MySolution_057_myqueue_056_dats__queue_make_nil__4__1():
  tmpret11__1 = None
  tmp12__1 = None
  tmp13__1 = None
  tmp14__1 = None
  tmp15__1 = None
  funlab_py = None
  tmplab_py = None
  #__patsflab_queue_make_nil
  tmp13__1 = None
  tmp12__1 = ats2pypre_ref(tmp13__1)
  tmp15__1 = None
  tmp14__1 = ats2pypre_ref(tmp15__1)
  tmpret11__1 = (tmp12__1, tmp14__1)
  return tmpret11__1


def theWorm_get():
  tmpret21 = None
  funlab_py = None
  tmplab_py = None
  #__patsflab_theWorm_get
  tmpret21 = Wormlike__statmp10
  return tmpret21


def theWorm_get_list():
  tmpret22 = None
  funlab_py = None
  tmplab_py = None
  #__patsflab_theWorm_get_list
  tmpret22 = _057_home_057_hwxi_057_Teaching_057_CS320_057_cs320_055_2016_055_summer_055_hwxi_057_assigns_057_6_057_Wormlike_057_MySolution_057_myqueue_056_dats__queue2rlist__8__1(Wormlike__statmp10)
  return tmpret22


def _057_home_057_hwxi_057_Teaching_057_CS320_057_cs320_055_2016_055_summer_055_hwxi_057_assigns_057_6_057_Wormlike_057_MySolution_057_myqueue_056_dats__queue2rlist__8__1(arg0):
  tmpret23__1 = None
  tmp24__1 = None
  tmp25__1 = None
  tmp26__1 = None
  tmp27__1 = None
  tmp28__1 = None
  funlab_py = None
  tmplab_py = None
  #__patsflab_queue2rlist
  tmp24__1 = arg0[0]
  tmp25__1 = arg0[1]
  tmp26__1 = ats2pypre_ref_get_elt(tmp24__1)
  tmp28__1 = ats2pypre_ref_get_elt(tmp25__1)
  tmp27__1 = ats2pypre_ML_list0_reverse(tmp28__1)
  tmpret23__1 = ats2pypre_ML_list0_append(tmp26__1, tmp27__1)
  return tmpret23__1


def theWorm_length():
  tmpret35 = None
  funlab_py = None
  tmplab_py = None
  #__patsflab_theWorm_length
  tmpret35 = _057_home_057_hwxi_057_Teaching_057_CS320_057_cs320_055_2016_055_summer_055_hwxi_057_assigns_057_6_057_Wormlike_057_MySolution_057_myqueue_056_dats__queue_size__11__1(Wormlike__statmp10)
  return tmpret35


def _057_home_057_hwxi_057_Teaching_057_CS320_057_cs320_055_2016_055_summer_055_hwxi_057_assigns_057_6_057_Wormlike_057_MySolution_057_myqueue_056_dats__queue_size__11__1(arg0):
  tmpret36__1 = None
  tmp37__1 = None
  tmp38__1 = None
  tmp39__1 = None
  tmp40__1 = None
  tmp41__1 = None
  tmp42__1 = None
  funlab_py = None
  tmplab_py = None
  #__patsflab_queue_size
  tmp37__1 = arg0[0]
  tmp38__1 = arg0[1]
  tmp40__1 = ats2pypre_ref_get_elt(tmp37__1)
  tmp39__1 = ats2pypre_ML_list0_length(tmp40__1)
  tmp42__1 = ats2pypre_ref_get_elt(tmp38__1)
  tmp41__1 = ats2pypre_ML_list0_length(tmp42__1)
  tmpret36__1 = ats2pypre_add_int1_int1(tmp39__1, tmp41__1)
  return tmpret36__1


def _057_home_057_hwxi_057_Teaching_057_CS320_057_cs320_055_2016_055_summer_055_hwxi_057_assigns_057_6_057_Wormlike_057_MySolution_057_Wormlike_056_sats__theWorm_decby1():
  tmpret50 = None
  funlab_py = None
  tmplab_py = None
  #__patsflab_theWorm_decby1
  tmpret50 = _057_home_057_hwxi_057_Teaching_057_CS320_057_cs320_055_2016_055_summer_055_hwxi_057_assigns_057_6_057_Wormlike_057_MySolution_057_myqueue_056_dats__queue_dequeue_opt__14__1(Wormlike__statmp10)
  return tmpret50


def _057_home_057_hwxi_057_Teaching_057_CS320_057_cs320_055_2016_055_summer_055_hwxi_057_assigns_057_6_057_Wormlike_057_MySolution_057_myqueue_056_dats__queue_dequeue_opt__14__1(arg0):
  tmpret51__1 = None
  tmp52__1 = None
  tmp53__1 = None
  tmp54__1 = None
  tmp55__1 = None
  tmp56__1 = None
  tmp57__1 = None
  tmp58__1 = None
  tmp60__1 = None
  tmp61__1 = None
  tmp62__1 = None
  funlab_py = None
  tmplab_py = None
  mbranch_1 = None
  mbranch_2 = None
  def __atstmplab0():
    nonlocal arg0
    nonlocal tmpret51__1, tmp52__1, tmp53__1, tmp54__1, tmp55__1, tmp56__1, tmp57__1, tmp58__1, tmp60__1, tmp61__1, tmp62__1
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1, mbranch_2
    tmplab_py = 0
    if(ATSCKptriscons(tmp53__1)): tmplab_py = 4 ; return#__atstmplab3
    __atstmplab1()
    return
  def __atstmplab1():
    nonlocal arg0
    nonlocal tmpret51__1, tmp52__1, tmp53__1, tmp54__1, tmp55__1, tmp56__1, tmp57__1, tmp58__1, tmp60__1, tmp61__1, tmp62__1
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1, mbranch_2
    tmplab_py = 0
    tmp56__1 = arg0[0]
    tmp58__1 = ats2pypre_ref_get_elt(tmp56__1)
    tmp57__1 = ats2pypre_ML_list0_reverse(tmp58__1)
    tmp60__1 = None
    ats2pypre_ref_set_elt(tmp56__1, tmp60__1)
    #ATScaseofseq_beg
    tmplab_py = 1
    while(1):
      mbranch_2.get(tmplab_py)()
      if (tmplab_py == 0): break
    #ATScaseofseq_end
    return
  def __atstmplab2():
    nonlocal arg0
    nonlocal tmpret51__1, tmp52__1, tmp53__1, tmp54__1, tmp55__1, tmp56__1, tmp57__1, tmp58__1, tmp60__1, tmp61__1, tmp62__1
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1, mbranch_2
    tmplab_py = 0
    if(ATSCKptrisnull(tmp53__1)): ATSINScaseof_fail("/home/hwxi/Teaching/CS320/cs320-2016-summer-hwxi/assigns/6/Wormlike/MySolution/myqueue.dats: 1728(line=126, offs=3) -- 2010(line=136, offs=42)");
    __atstmplab3()
    return
  def __atstmplab3():
    nonlocal arg0
    nonlocal tmpret51__1, tmp52__1, tmp53__1, tmp54__1, tmp55__1, tmp56__1, tmp57__1, tmp58__1, tmp60__1, tmp61__1, tmp62__1
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1, mbranch_2
    tmplab_py = 0
    tmp54__1 = tmp53__1[0]
    tmp55__1 = tmp53__1[1]
    ats2pypre_ref_set_elt(tmp52__1, tmp55__1)
    tmpret51__1 = (tmp54__1, )
    return
  def __atstmplab4():
    nonlocal arg0
    nonlocal tmpret51__1, tmp52__1, tmp53__1, tmp54__1, tmp55__1, tmp56__1, tmp57__1, tmp58__1, tmp60__1, tmp61__1, tmp62__1
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1, mbranch_2
    tmplab_py = 0
    if(ATSCKptriscons(tmp57__1)): tmplab_py = 4 ; return#__atstmplab7
    __atstmplab5()
    return
  def __atstmplab5():
    nonlocal arg0
    nonlocal tmpret51__1, tmp52__1, tmp53__1, tmp54__1, tmp55__1, tmp56__1, tmp57__1, tmp58__1, tmp60__1, tmp61__1, tmp62__1
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1, mbranch_2
    tmplab_py = 0
    tmpret51__1 = None
    return
  def __atstmplab6():
    nonlocal arg0
    nonlocal tmpret51__1, tmp52__1, tmp53__1, tmp54__1, tmp55__1, tmp56__1, tmp57__1, tmp58__1, tmp60__1, tmp61__1, tmp62__1
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1, mbranch_2
    tmplab_py = 0
    __atstmplab7()
    return
  def __atstmplab7():
    nonlocal arg0
    nonlocal tmpret51__1, tmp52__1, tmp53__1, tmp54__1, tmp55__1, tmp56__1, tmp57__1, tmp58__1, tmp60__1, tmp61__1, tmp62__1
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1, mbranch_2
    tmplab_py = 0
    tmp61__1 = tmp57__1[0]
    tmp62__1 = tmp57__1[1]
    ats2pypre_ref_set_elt(tmp52__1, tmp62__1)
    tmpret51__1 = (tmp61__1, )
    return
  mbranch_1 = { 1: __atstmplab0, 2: __atstmplab1, 3: __atstmplab2, 4: __atstmplab3 }
  mbranch_2 = { 1: __atstmplab4, 2: __atstmplab5, 3: __atstmplab6, 4: __atstmplab7 }
  #__patsflab_queue_dequeue_opt
  tmp52__1 = arg0[1]
  tmp53__1 = ats2pypre_ref_get_elt(tmp52__1)
  #ATScaseofseq_beg
  tmplab_py = 1
  while(1):
    mbranch_1.get(tmplab_py)()
    if (tmplab_py == 0): break
  #ATScaseofseq_end
  return tmpret51__1


def _057_home_057_hwxi_057_Teaching_057_CS320_057_cs320_055_2016_055_summer_055_hwxi_057_assigns_057_6_057_Wormlike_057_MySolution_057_Wormlike_056_sats__theWorm_incby1(arg0):
  funlab_py = None
  tmplab_py = None
  #__patsflab_theWorm_incby1
  _057_home_057_hwxi_057_Teaching_057_CS320_057_cs320_055_2016_055_summer_055_hwxi_057_assigns_057_6_057_Wormlike_057_MySolution_057_myqueue_056_dats__queue_enqueue__17__1(Wormlike__statmp10, arg0)
  return#_void


def _057_home_057_hwxi_057_Teaching_057_CS320_057_cs320_055_2016_055_summer_055_hwxi_057_assigns_057_6_057_Wormlike_057_MySolution_057_myqueue_056_dats__queue_enqueue__17__1(arg0, arg1):
  tmp81__1 = None
  tmp82__1 = None
  tmp83__1 = None
  funlab_py = None
  tmplab_py = None
  #__patsflab_queue_enqueue
  tmp81__1 = arg0[0]
  tmp83__1 = ats2pypre_ref_get_elt(tmp81__1)
  tmp82__1 = (arg1, tmp83__1)
  ats2pypre_ref_set_elt(tmp81__1, tmp82__1)
  return#_void


def theWorm_insert():
  tmp89 = None
  tmp90 = None
  tmp101 = None
  tmp102 = None
  tmp103 = None
  funlab_py = None
  tmplab_py = None
  #__patsflab_theWorm_insert
  tmp89 = theWorm_get()
  tmp90 = _057_home_057_hwxi_057_Teaching_057_CS320_057_cs320_055_2016_055_summer_055_hwxi_057_assigns_057_6_057_Wormlike_057_MySolution_057_myqueue_056_dats__queue2list2__20__1(tmp89)
  tmp101 = tmp90[0]
  tmp102 = tmp90[1]
  tmp103 = theGamebd_get()
  Wormlike__aux_22(tmp103, tmp101)
  Wormlike__aux_22(tmp103, tmp102)
  return#_void


def _057_home_057_hwxi_057_Teaching_057_CS320_057_cs320_055_2016_055_summer_055_hwxi_057_assigns_057_6_057_Wormlike_057_MySolution_057_myqueue_056_dats__queue2list2__20__1(arg0):
  tmpret91__1 = None
  tmp92__1 = None
  tmp93__1 = None
  tmp94__1 = None
  tmp95__1 = None
  funlab_py = None
  tmplab_py = None
  #__patsflab_queue2list2
  tmp92__1 = arg0[0]
  tmp93__1 = arg0[1]
  tmp94__1 = ats2pypre_ref_get_elt(tmp92__1)
  tmp95__1 = ats2pypre_ref_get_elt(tmp93__1)
  tmpret91__1 = (tmp94__1, tmp95__1)
  return tmpret91__1


def Wormlike__aux_22(env0, arg0):
  apy0 = None
  tmp105 = None
  tmp106 = None
  tmp107 = None
  tmp108 = None
  tmp110 = None
  funlab_py = None
  tmplab_py = None
  mbranch_1 = None
  def __atstmplab8():
    nonlocal env0, arg0
    nonlocal apy0, tmp105, tmp106, tmp107, tmp108, tmp110
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1
    tmplab_py = 0
    if(ATSCKptriscons(arg0)): tmplab_py = 4 ; return#__atstmplab11
    __atstmplab9()
    return
  def __atstmplab9():
    nonlocal env0, arg0
    nonlocal apy0, tmp105, tmp106, tmp107, tmp108, tmp110
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1
    tmplab_py = 0
    None#ATSINSmove_void
    return
  def __atstmplab10():
    nonlocal env0, arg0
    nonlocal apy0, tmp105, tmp106, tmp107, tmp108, tmp110
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1
    tmplab_py = 0
    __atstmplab11()
    return
  def __atstmplab11():
    nonlocal env0, arg0
    nonlocal apy0, tmp105, tmp106, tmp107, tmp108, tmp110
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1
    tmplab_py = 0
    tmp105 = arg0[0]
    tmp106 = arg0[1]
    tmp107 = tmp105[0]
    tmp108 = tmp105[1]
    tmp110 = (0, )
    ats2pypre_mtrxszref_set_at(env0, tmp107, tmp108, tmp110)
    #ATStailcalseq_beg
    apy0 = tmp106
    arg0 = apy0
    funlab_py = 1 #__patsflab_Wormlike__aux_22
    #ATStailcalseq_end
    return
  mbranch_1 = { 1: __atstmplab8, 2: __atstmplab9, 3: __atstmplab10, 4: __atstmplab11 }
  while(1):
    funlab_py = 0
    #__patsflab_Wormlike__aux_22
    #ATScaseofseq_beg
    tmplab_py = 1
    while(1):
      mbranch_1.get(tmplab_py)()
      if (tmplab_py == 0): break
    #ATScaseofseq_end
    if (funlab_py == 0): break
  return#_void


def theWorm_remove():
  tmp113 = None
  tmp114 = None
  tmp120 = None
  tmp121 = None
  tmp122 = None
  funlab_py = None
  tmplab_py = None
  #__patsflab_theWorm_remove
  tmp113 = theWorm_get()
  tmp114 = _057_home_057_hwxi_057_Teaching_057_CS320_057_cs320_055_2016_055_summer_055_hwxi_057_assigns_057_6_057_Wormlike_057_MySolution_057_myqueue_056_dats__queue2list2__20__2(tmp113)
  tmp120 = tmp114[0]
  tmp121 = tmp114[1]
  tmp122 = theGamebd_get()
  Wormlike__aux_25(tmp122, tmp120)
  Wormlike__aux_25(tmp122, tmp121)
  return#_void


def _057_home_057_hwxi_057_Teaching_057_CS320_057_cs320_055_2016_055_summer_055_hwxi_057_assigns_057_6_057_Wormlike_057_MySolution_057_myqueue_056_dats__queue2list2__20__2(arg0):
  tmpret91__2 = None
  tmp92__2 = None
  tmp93__2 = None
  tmp94__2 = None
  tmp95__2 = None
  funlab_py = None
  tmplab_py = None
  #__patsflab_queue2list2
  tmp92__2 = arg0[0]
  tmp93__2 = arg0[1]
  tmp94__2 = ats2pypre_ref_get_elt(tmp92__2)
  tmp95__2 = ats2pypre_ref_get_elt(tmp93__2)
  tmpret91__2 = (tmp94__2, tmp95__2)
  return tmpret91__2


def Wormlike__aux_25(env0, arg0):
  apy0 = None
  tmp124 = None
  tmp125 = None
  tmp126 = None
  tmp127 = None
  tmp129 = None
  funlab_py = None
  tmplab_py = None
  mbranch_1 = None
  def __atstmplab12():
    nonlocal env0, arg0
    nonlocal apy0, tmp124, tmp125, tmp126, tmp127, tmp129
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1
    tmplab_py = 0
    if(ATSCKptriscons(arg0)): tmplab_py = 4 ; return#__atstmplab15
    __atstmplab13()
    return
  def __atstmplab13():
    nonlocal env0, arg0
    nonlocal apy0, tmp124, tmp125, tmp126, tmp127, tmp129
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1
    tmplab_py = 0
    None#ATSINSmove_void
    return
  def __atstmplab14():
    nonlocal env0, arg0
    nonlocal apy0, tmp124, tmp125, tmp126, tmp127, tmp129
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1
    tmplab_py = 0
    __atstmplab15()
    return
  def __atstmplab15():
    nonlocal env0, arg0
    nonlocal apy0, tmp124, tmp125, tmp126, tmp127, tmp129
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1
    tmplab_py = 0
    tmp124 = arg0[0]
    tmp125 = arg0[1]
    tmp126 = tmp124[0]
    tmp127 = tmp124[1]
    tmp129 = None
    ats2pypre_mtrxszref_set_at(env0, tmp126, tmp127, tmp129)
    #ATStailcalseq_beg
    apy0 = tmp125
    arg0 = apy0
    funlab_py = 1 #__patsflab_Wormlike__aux_25
    #ATStailcalseq_end
    return
  mbranch_1 = { 1: __atstmplab12, 2: __atstmplab13, 3: __atstmplab14, 4: __atstmplab15 }
  while(1):
    funlab_py = 0
    #__patsflab_Wormlike__aux_25
    #ATScaseofseq_beg
    tmplab_py = 1
    while(1):
      mbranch_1.get(tmplab_py)()
      if (tmplab_py == 0): break
    #ATScaseofseq_end
    if (funlab_py == 0): break
  return#_void


def theWorm_last_opt():
  tmpret131 = None
  tmp145 = None
  funlab_py = None
  tmplab_py = None
  #__patsflab_theWorm_last_opt
  tmp145 = theWorm_get()
  tmpret131 = _057_home_057_hwxi_057_Teaching_057_CS320_057_cs320_055_2016_055_summer_055_hwxi_057_assigns_057_6_057_Wormlike_057_MySolution_057_myqueue_056_dats__queue_last_opt__27__1(tmp145)
  return tmpret131


def _057_home_057_hwxi_057_Teaching_057_CS320_057_cs320_055_2016_055_summer_055_hwxi_057_assigns_057_6_057_Wormlike_057_MySolution_057_myqueue_056_dats__queue_last_opt__27__1(arg0):
  tmpret132__1 = None
  tmp133__1 = None
  tmp134__1 = None
  tmp135__1 = None
  tmp137__1 = None
  tmp138__1 = None
  funlab_py = None
  tmplab_py = None
  mbranch_1 = None
  def __atstmplab16():
    nonlocal arg0
    nonlocal tmpret132__1, tmp133__1, tmp134__1, tmp135__1, tmp137__1, tmp138__1
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1
    tmplab_py = 0
    if(ATSCKptrisnull(tmp134__1)): tmplab_py = 4 ; return#__atstmplab19
    __atstmplab17()
    return
  def __atstmplab17():
    nonlocal arg0
    nonlocal tmpret132__1, tmp133__1, tmp134__1, tmp135__1, tmp137__1, tmp138__1
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1
    tmplab_py = 0
    tmp135__1 = tmp134__1[0]
    tmpret132__1 = (tmp135__1, )
    return
  def __atstmplab18():
    nonlocal arg0
    nonlocal tmpret132__1, tmp133__1, tmp134__1, tmp135__1, tmp137__1, tmp138__1
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1
    tmplab_py = 0
    __atstmplab19()
    return
  def __atstmplab19():
    nonlocal arg0
    nonlocal tmpret132__1, tmp133__1, tmp134__1, tmp135__1, tmp137__1, tmp138__1
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1
    tmplab_py = 0
    tmp137__1 = arg0[1]
    tmp138__1 = ats2pypre_ref_get_elt(tmp137__1)
    tmpret132__1 = ats2pypre_ML_list0_last_opt(tmp138__1)
    return
  mbranch_1 = { 1: __atstmplab16, 2: __atstmplab17, 3: __atstmplab18, 4: __atstmplab19 }
  #__patsflab_queue_last_opt
  tmp133__1 = arg0[0]
  tmp134__1 = ats2pypre_ref_get_elt(tmp133__1)
  #ATScaseofseq_beg
  tmplab_py = 1
  while(1):
    mbranch_1.get(tmplab_py)()
    if (tmplab_py == 0): break
  #ATScaseofseq_end
  return tmpret132__1


def _057_home_057_hwxi_057_Teaching_057_CS320_057_cs320_055_2016_055_summer_055_hwxi_057_assigns_057_6_057_Wormlike_057_MySolution_057_Wormlike_main_056_dats__theWorm_next(arg0, arg1, arg2):
  tmpret146 = None
  tmp147 = None
  tmp148 = None
  tmp149 = None
  tmp150 = None
  tmp151 = None
  tmp152 = None
  tmp153 = None
  tmp154 = None
  tmp155 = None
  tmp156 = None
  tmp157 = None
  tmp158 = None
  tmp159 = None
  tmp160 = None
  tmp161 = None
  tmp162 = None
  tmp163 = None
  funlab_py = None
  tmplab_py = None
  mbranch_1 = None
  mbranch_2 = None
  def __atstmplab20():
    nonlocal arg0, arg1, arg2
    nonlocal tmpret146, tmp147, tmp148, tmp149, tmp150, tmp151, tmp152, tmp153, tmp154, tmp155, tmp156, tmp157, tmp158, tmp159, tmp160, tmp161, tmp162, tmp163
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1, mbranch_2
    tmplab_py = 0
    if(ATSCKptriscons(tmp148)): tmplab_py = 4 ; return#__atstmplab23
    __atstmplab21()
    return
  def __atstmplab21():
    nonlocal arg0, arg1, arg2
    nonlocal tmpret146, tmp147, tmp148, tmp149, tmp150, tmp151, tmp152, tmp153, tmp154, tmp155, tmp156, tmp157, tmp158, tmp159, tmp160, tmp161, tmp162, tmp163
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1, mbranch_2
    tmplab_py = 0
    tmp150 = (0, 0)
    tmpret146 = (tmp150, )
    return
  def __atstmplab22():
    nonlocal arg0, arg1, arg2
    nonlocal tmpret146, tmp147, tmp148, tmp149, tmp150, tmp151, tmp152, tmp153, tmp154, tmp155, tmp156, tmp157, tmp158, tmp159, tmp160, tmp161, tmp162, tmp163
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1, mbranch_2
    tmplab_py = 0
    __atstmplab23()
    return
  def __atstmplab23():
    nonlocal arg0, arg1, arg2
    nonlocal tmpret146, tmp147, tmp148, tmp149, tmp150, tmp151, tmp152, tmp153, tmp154, tmp155, tmp156, tmp157, tmp158, tmp159, tmp160, tmp161, tmp162, tmp163
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1, mbranch_2
    tmplab_py = 0
    tmp149 = tmp148[0]
    tmp151 = tmp149[0]
    tmp152 = tmp149[1]
    tmp154 = ats2pypre_gt_int0_int0(arg1, 0)
    if (tmp154):
      tmp155 = ats2pypre_gt_int0_int0(arg0, 0)
      if (tmp155):
        tmp153 = _057_home_057_hwxi_057_Teaching_057_CS320_057_cs320_055_2016_055_summer_055_hwxi_057_assigns_057_6_057_Wormlike_057_MySolution_057_Wormlike_056_sats__succ_row(tmp151)
      else:
        tmp153 = _057_home_057_hwxi_057_Teaching_057_CS320_057_cs320_055_2016_055_summer_055_hwxi_057_assigns_057_6_057_Wormlike_057_MySolution_057_Wormlike_056_sats__pred_row(tmp151)
      #endif
    else:
      tmp153 = tmp151
    #endif
    tmp157 = ats2pypre_gt_int0_int0(arg2, 0)
    if (tmp157):
      tmp158 = ats2pypre_gt_int0_int0(arg0, 0)
      if (tmp158):
        tmp156 = _057_home_057_hwxi_057_Teaching_057_CS320_057_cs320_055_2016_055_summer_055_hwxi_057_assigns_057_6_057_Wormlike_057_MySolution_057_Wormlike_056_sats__succ_col(tmp152)
      else:
        tmp156 = _057_home_057_hwxi_057_Teaching_057_CS320_057_cs320_055_2016_055_summer_055_hwxi_057_assigns_057_6_057_Wormlike_057_MySolution_057_Wormlike_056_sats__pred_col(tmp152)
      #endif
    else:
      tmp156 = tmp152
    #endif
    tmp159 = ats2pypre_mtrxszref_get_at(tmp147, tmp153, tmp156)
    #ATScaseofseq_beg
    tmplab_py = 1
    while(1):
      mbranch_2.get(tmplab_py)()
      if (tmplab_py == 0): break
    #ATScaseofseq_end
    return
  def __atstmplab24():
    nonlocal arg0, arg1, arg2
    nonlocal tmpret146, tmp147, tmp148, tmp149, tmp150, tmp151, tmp152, tmp153, tmp154, tmp155, tmp156, tmp157, tmp158, tmp159, tmp160, tmp161, tmp162, tmp163
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1, mbranch_2
    tmplab_py = 0
    if(ATSCKptriscons(tmp159)): tmplab_py = 4 ; return#__atstmplab27
    __atstmplab25()
    return
  def __atstmplab25():
    nonlocal arg0, arg1, arg2
    nonlocal tmpret146, tmp147, tmp148, tmp149, tmp150, tmp151, tmp152, tmp153, tmp154, tmp155, tmp156, tmp157, tmp158, tmp159, tmp160, tmp161, tmp162, tmp163
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1, mbranch_2
    tmplab_py = 0
    tmp161 = (tmp153, tmp156)
    tmpret146 = (tmp161, )
    return
  def __atstmplab26():
    nonlocal arg0, arg1, arg2
    nonlocal tmpret146, tmp147, tmp148, tmp149, tmp150, tmp151, tmp152, tmp153, tmp154, tmp155, tmp156, tmp157, tmp158, tmp159, tmp160, tmp161, tmp162, tmp163
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1, mbranch_2
    tmplab_py = 0
    __atstmplab27()
    return
  def __atstmplab27():
    nonlocal arg0, arg1, arg2
    nonlocal tmpret146, tmp147, tmp148, tmp149, tmp150, tmp151, tmp152, tmp153, tmp154, tmp155, tmp156, tmp157, tmp158, tmp159, tmp160, tmp161, tmp162, tmp163
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1, mbranch_2
    tmplab_py = 0
    tmp160 = tmp159[0]
    tmp162 = ats2pypre_lte_int0_int0(tmp160, 0)
    if (tmp162):
      tmpret146 = None
    else:
      tmp163 = (tmp153, tmp156)
      tmpret146 = (tmp163, )
    #endif
    return
  mbranch_1 = { 1: __atstmplab20, 2: __atstmplab21, 3: __atstmplab22, 4: __atstmplab23 }
  mbranch_2 = { 1: __atstmplab24, 2: __atstmplab25, 3: __atstmplab26, 4: __atstmplab27 }
  #__patsflab_theWorm_next
  tmp147 = theGamebd_get()
  tmp148 = theWorm_last_opt()
  #ATScaseofseq_beg
  tmplab_py = 1
  while(1):
    mbranch_1.get(tmplab_py)()
    if (tmplab_py == 0): break
  #ATScaseofseq_end
  return tmpret146


def _057_home_057_hwxi_057_Teaching_057_CS320_057_cs320_055_2016_055_summer_055_hwxi_057_assigns_057_6_057_Wormlike_057_MySolution_057_Wormlike_main_056_dats__theWorm_move_with(arg0):
  tmpret164 = None
  tmp166 = None
  tmp167 = None
  tmp169 = None
  tmp172 = None
  tmp173 = None
  funlab_py = None
  tmplab_py = None
  mbranch_1 = None
  def __atstmplab28():
    nonlocal arg0
    nonlocal tmpret164, tmp166, tmp167, tmp169, tmp172, tmp173
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1
    tmplab_py = 0
    if(ATSCKptriscons(arg0)): tmplab_py = 4 ; return#__atstmplab31
    __atstmplab29()
    return
  def __atstmplab29():
    nonlocal arg0
    nonlocal tmpret164, tmp166, tmp167, tmp169, tmp172, tmp173
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1
    tmplab_py = 0
    tmp167 = _057_home_057_hwxi_057_Teaching_057_CS320_057_cs320_055_2016_055_summer_055_hwxi_057_assigns_057_6_057_Wormlike_057_MySolution_057_Wormlike_056_sats__theWorm_decby1()
    theWorm_insert()
    tmpret164 = 0
    return
  def __atstmplab30():
    nonlocal arg0
    nonlocal tmpret164, tmp166, tmp167, tmp169, tmp172, tmp173
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1
    tmplab_py = 0
    __atstmplab31()
    return
  def __atstmplab31():
    nonlocal arg0
    nonlocal tmpret164, tmp166, tmp167, tmp169, tmp172, tmp173
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1
    tmplab_py = 0
    tmp166 = arg0[0]
    tmp169 = theWorm_length()
    _057_home_057_hwxi_057_Teaching_057_CS320_057_cs320_055_2016_055_summer_055_hwxi_057_assigns_057_6_057_Wormlike_057_MySolution_057_Wormlike_056_sats__theWorm_incby1(tmp166)
    tmp172 = ats2pypre_gt_int0_int0(tmp169, 48)
    if (tmp172):
      tmp173 = _057_home_057_hwxi_057_Teaching_057_CS320_057_cs320_055_2016_055_summer_055_hwxi_057_assigns_057_6_057_Wormlike_057_MySolution_057_Wormlike_056_sats__theWorm_decby1()
      None#ATSINSmove_void
    else:
      None#ATSINSmove_void
    #endif
    theWorm_insert()
    tmpret164 = 1
    return
  mbranch_1 = { 1: __atstmplab28, 2: __atstmplab29, 3: __atstmplab30, 4: __atstmplab31 }
  #__patsflab_theWorm_move_with
  theWorm_remove()
  #ATScaseofseq_beg
  tmplab_py = 1
  while(1):
    mbranch_1.get(tmplab_py)()
    if (tmplab_py == 0): break
  #ATScaseofseq_end
  return tmpret164


def _057_home_057_hwxi_057_Teaching_057_CS320_057_cs320_055_2016_055_summer_055_hwxi_057_assigns_057_6_057_Wormlike_057_MySolution_057_Wormlike_main_056_dats__theWorm_next_random():
  tmpret175 = None
  tmp179 = None
  tmp180 = None
  tmp181 = None
  funlab_py = None
  tmplab_py = None
  #__patsflab_theWorm_next_random
  tmp179 = Wormlike__dirget_32()
  tmp180 = Wormlike__dirget_32()
  tmp181 = ats2pypre_sub_int0_int0(1, tmp180)
  tmpret175 = _057_home_057_hwxi_057_Teaching_057_CS320_057_cs320_055_2016_055_summer_055_hwxi_057_assigns_057_6_057_Wormlike_057_MySolution_057_Wormlike_main_056_dats__theWorm_next(tmp179, tmp180, tmp181)
  return tmpret175


def Wormlike__dirget_32():
  tmpret176 = None
  tmp177 = None
  tmp178 = None
  funlab_py = None
  tmplab_py = None
  #__patsflab_Wormlike__dirget_32
  tmp178 = ats2pylibc_random_random()
  tmp177 = ats2pypre_gte_double_double(tmp178, 0.5)
  if (tmp177):
    tmpret176 = 1
  else:
    tmpret176 = 0
  #endif
  return tmpret176


def theWorm_move_up():
  tmpret182 = None
  tmp183 = None
  funlab_py = None
  tmplab_py = None
  #__patsflab_theWorm_move_up
  tmp183 = _057_home_057_hwxi_057_Teaching_057_CS320_057_cs320_055_2016_055_summer_055_hwxi_057_assigns_057_6_057_Wormlike_057_MySolution_057_Wormlike_main_056_dats__theWorm_next(0, 0, 1)
  tmpret182 = _057_home_057_hwxi_057_Teaching_057_CS320_057_cs320_055_2016_055_summer_055_hwxi_057_assigns_057_6_057_Wormlike_057_MySolution_057_Wormlike_main_056_dats__theWorm_move_with(tmp183)
  return tmpret182


def theWorm_move_down():
  tmpret184 = None
  tmp185 = None
  funlab_py = None
  tmplab_py = None
  #__patsflab_theWorm_move_down
  tmp185 = _057_home_057_hwxi_057_Teaching_057_CS320_057_cs320_055_2016_055_summer_055_hwxi_057_assigns_057_6_057_Wormlike_057_MySolution_057_Wormlike_main_056_dats__theWorm_next(1, 0, 1)
  tmpret184 = _057_home_057_hwxi_057_Teaching_057_CS320_057_cs320_055_2016_055_summer_055_hwxi_057_assigns_057_6_057_Wormlike_057_MySolution_057_Wormlike_main_056_dats__theWorm_move_with(tmp185)
  return tmpret184


def theWorm_move_left():
  tmpret186 = None
  tmp187 = None
  funlab_py = None
  tmplab_py = None
  #__patsflab_theWorm_move_left
  tmp187 = _057_home_057_hwxi_057_Teaching_057_CS320_057_cs320_055_2016_055_summer_055_hwxi_057_assigns_057_6_057_Wormlike_057_MySolution_057_Wormlike_main_056_dats__theWorm_next(0, 1, 0)
  tmpret186 = _057_home_057_hwxi_057_Teaching_057_CS320_057_cs320_055_2016_055_summer_055_hwxi_057_assigns_057_6_057_Wormlike_057_MySolution_057_Wormlike_main_056_dats__theWorm_move_with(tmp187)
  return tmpret186


def theWorm_move_right():
  tmpret188 = None
  tmp189 = None
  funlab_py = None
  tmplab_py = None
  #__patsflab_theWorm_move_right
  tmp189 = _057_home_057_hwxi_057_Teaching_057_CS320_057_cs320_055_2016_055_summer_055_hwxi_057_assigns_057_6_057_Wormlike_057_MySolution_057_Wormlike_main_056_dats__theWorm_next(1, 1, 0)
  tmpret188 = _057_home_057_hwxi_057_Teaching_057_CS320_057_cs320_055_2016_055_summer_055_hwxi_057_assigns_057_6_057_Wormlike_057_MySolution_057_Wormlike_main_056_dats__theWorm_move_with(tmp189)
  return tmpret188


def theWorm_move_random():
  tmpret190 = None
  tmp191 = None
  funlab_py = None
  tmplab_py = None
  #__patsflab_theWorm_move_random
  tmp191 = _057_home_057_hwxi_057_Teaching_057_CS320_057_cs320_055_2016_055_summer_055_hwxi_057_assigns_057_6_057_Wormlike_057_MySolution_057_Wormlike_main_056_dats__theWorm_next_random()
  tmpret190 = _057_home_057_hwxi_057_Teaching_057_CS320_057_cs320_055_2016_055_summer_055_hwxi_057_assigns_057_6_057_Wormlike_057_MySolution_057_Wormlike_main_056_dats__theWorm_move_with(tmp191)
  return tmpret190


def theGamebd_get():
  tmpret194 = None
  funlab_py = None
  tmplab_py = None
  #__patsflab_theGamebd_get
  tmpret194 = Wormlike__statmp192
  return tmpret194


def theVisitbd_get():
  tmpret196 = None
  funlab_py = None
  tmplab_py = None
  #__patsflab_theVisitbd_get
  tmpret196 = Wormlike__statmp195
  return tmpret196


def theVisitbd_reset():
  tmp198 = None
  funlab_py = None
  tmplab_py = None
  #__patsflab_theVisitbd_reset
  tmp198 = theVisitbd_get()
  ats2pypre_mtrxszref_foreach_cloref(tmp198, Wormlike__patsfun_41__closurerize(tmp198))
  return#_void


def Wormlike__patsfun_41(env0, arg0, arg1):
  funlab_py = None
  tmplab_py = None
  #__patsflab_Wormlike__patsfun_41
  ats2pypre_mtrxszref_set_at(env0, arg0, arg1, 0)
  return#_void


def Wormlike__xnode2color_49(arg0):
  tmpret228 = None
  tmp229 = None
  tmp230 = None
  tmp231 = None
  funlab_py = None
  tmplab_py = None
  mbranch_1 = None
  def __atstmplab32():
    nonlocal arg0
    nonlocal tmpret228, tmp229, tmp230, tmp231
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1
    tmplab_py = 0
    if(ATSCKptriscons(arg0)): tmplab_py = 4 ; return#__atstmplab35
    __atstmplab33()
    return
  def __atstmplab33():
    nonlocal arg0
    nonlocal tmpret228, tmp229, tmp230, tmp231
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1
    tmplab_py = 0
    tmpret228 = Wormlike__statmp226
    return
  def __atstmplab34():
    nonlocal arg0
    nonlocal tmpret228, tmp229, tmp230, tmp231
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1
    tmplab_py = 0
    __atstmplab35()
    return
  def __atstmplab35():
    nonlocal arg0
    nonlocal tmpret228, tmp229, tmp230, tmp231
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1
    tmplab_py = 0
    tmp229 = arg0[0]
    tmp230 = ats2pypre_eq_int0_int0(tmp229, 0)
    if (tmp230):
      tmpret228 = Wormlike__statmp224
    else:
      tmp231 = ats2pypre_eq_int0_int0(tmp229, 1)
      if (tmp231):
        tmpret228 = Wormlike__statmp227
      else:
        tmpret228 = Wormlike__statmp225
      #endif
    #endif
    return
  mbranch_1 = { 1: __atstmplab32, 2: __atstmplab33, 3: __atstmplab34, 4: __atstmplab35 }
  #__patsflab_Wormlike__xnode2color_49
  #ATScaseofseq_beg
  tmplab_py = 1
  while(1):
    mbranch_1.get(tmplab_py)()
    if (tmplab_py == 0): break
  #ATScaseofseq_end
  return tmpret228


def Wormlike__main():
  tmp235 = None
  tmp236 = None
  tmp237 = None
  tmp238 = None
  tmp239 = None
  tmp240 = None
  tmp263 = None
  tmp265 = None
  tmp324 = None
  tmp325 = None
  funlab_py = None
  tmplab_py = None
  #__patsflab_Wormlike__main
  ats2pypre_print_string("Hello from [Wormlike]!")
  ats2pypre_print_newline()
  tmp235 = ats2py_pygame_pygame_init_ret()
  tmp236 = (660, 660)
  tmp237 = ats2py_pygame_display_set_mode_resolution(tmp236)
  tmp238 = ats2py_pygame_surface_fill_(tmp237, Wormlike__statmp224)
  tmp240 = ats2py_pygame_surface_get_size(tmp237)
  tmp239 = ats2py_pygame_surface_make_flags_depth(tmp240, pygame.SRCALPHA, 32)
  tmp263 = ats2pylibc_random_random()
  tmp265 = ats2pypre_lte_double_double(tmp263, 0.5)
  if (tmp265):
    theWormlike_scene1()
  else:
    theWormlike_scene2()
  #endif
  theWormlike_bonus_rand(50)
  tmp324 = 5
  tmp325 = _057_home_057_hwxi_057_Teaching_057_CS320_057_cs320_055_2016_055_summer_055_hwxi_057_assigns_057_6_057_Wormlike_057_MySolution_057_Wormlike_056_sats__kstream_make(100)
  Wormlike__loop_53(tmp237, tmp239, tmp324, tmp325)
  ats2py_pygame_pygame_quit()
  return#_void


def Wormlike__loop_53(env0, env1, arg0, arg1):
  apy0 = None
  apy1 = None
  tmp268 = None
  tmp269 = None
  tmp270 = None
  tmp271 = None
  tmp272 = None
  tmp323 = None
  funlab_py = None
  tmplab_py = None
  mbranch_1 = None
  mbranch_2 = None
  def __atstmplab40():
    nonlocal env0, env1, arg0, arg1
    nonlocal apy0, apy1, tmp268, tmp269, tmp270, tmp271, tmp272, tmp323
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1, mbranch_2
    tmplab_py = 0
    if not(ATSCKpat_con0(tmp269, 0)): tmplab_py = 3 ; return#__atstmplab42
    __atstmplab41()
    return
  def __atstmplab41():
    nonlocal env0, env1, arg0, arg1
    nonlocal apy0, apy1, tmp268, tmp269, tmp270, tmp271, tmp272, tmp323
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1, mbranch_2
    tmplab_py = 0
    tmp271 = arg0
    return
  def __atstmplab42():
    nonlocal env0, env1, arg0, arg1
    nonlocal apy0, apy1, tmp268, tmp269, tmp270, tmp271, tmp272, tmp323
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1, mbranch_2
    tmplab_py = 0
    tmp271 = tmp269
    return
  def __atstmplab43():
    nonlocal env0, env1, arg0, arg1
    nonlocal apy0, apy1, tmp268, tmp269, tmp270, tmp271, tmp272, tmp323
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1, mbranch_2
    tmplab_py = 0
    if not(ATSCKpat_con0(tmp271, 0)): tmplab_py = 3 ; return#__atstmplab45
    __atstmplab44()
    return
  def __atstmplab44():
    nonlocal env0, env1, arg0, arg1
    nonlocal apy0, apy1, tmp268, tmp269, tmp270, tmp271, tmp272, tmp323
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1, mbranch_2
    tmplab_py = 0
    tmp272 = 0
    return
  def __atstmplab45():
    nonlocal env0, env1, arg0, arg1
    nonlocal apy0, apy1, tmp268, tmp269, tmp270, tmp271, tmp272, tmp323
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1, mbranch_2
    tmplab_py = 0
    if not(ATSCKpat_con0(tmp271, 1)): tmplab_py = 5 ; return#__atstmplab47
    __atstmplab46()
    return
  def __atstmplab46():
    nonlocal env0, env1, arg0, arg1
    nonlocal apy0, apy1, tmp268, tmp269, tmp270, tmp271, tmp272, tmp323
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1, mbranch_2
    tmplab_py = 0
    tmp272 = ats2pypre_neg_int0(1)
    return
  def __atstmplab47():
    nonlocal env0, env1, arg0, arg1
    nonlocal apy0, apy1, tmp268, tmp269, tmp270, tmp271, tmp272, tmp323
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1, mbranch_2
    tmplab_py = 0
    if not(ATSCKpat_con0(tmp271, 2)): tmplab_py = 7 ; return#__atstmplab49
    __atstmplab48()
    return
  def __atstmplab48():
    nonlocal env0, env1, arg0, arg1
    nonlocal apy0, apy1, tmp268, tmp269, tmp270, tmp271, tmp272, tmp323
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1, mbranch_2
    tmplab_py = 0
    tmp272 = theWorm_move_up()
    return
  def __atstmplab49():
    nonlocal env0, env1, arg0, arg1
    nonlocal apy0, apy1, tmp268, tmp269, tmp270, tmp271, tmp272, tmp323
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1, mbranch_2
    tmplab_py = 0
    if not(ATSCKpat_con0(tmp271, 3)): tmplab_py = 9 ; return#__atstmplab51
    __atstmplab50()
    return
  def __atstmplab50():
    nonlocal env0, env1, arg0, arg1
    nonlocal apy0, apy1, tmp268, tmp269, tmp270, tmp271, tmp272, tmp323
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1, mbranch_2
    tmplab_py = 0
    tmp272 = theWorm_move_down()
    return
  def __atstmplab51():
    nonlocal env0, env1, arg0, arg1
    nonlocal apy0, apy1, tmp268, tmp269, tmp270, tmp271, tmp272, tmp323
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1, mbranch_2
    tmplab_py = 0
    if not(ATSCKpat_con0(tmp271, 4)): tmplab_py = 11 ; return#__atstmplab53
    __atstmplab52()
    return
  def __atstmplab52():
    nonlocal env0, env1, arg0, arg1
    nonlocal apy0, apy1, tmp268, tmp269, tmp270, tmp271, tmp272, tmp323
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1, mbranch_2
    tmplab_py = 0
    tmp272 = theWorm_move_left()
    return
  def __atstmplab53():
    nonlocal env0, env1, arg0, arg1
    nonlocal apy0, apy1, tmp268, tmp269, tmp270, tmp271, tmp272, tmp323
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1, mbranch_2
    tmplab_py = 0
    if not(ATSCKpat_con0(tmp271, 5)): tmplab_py = 13 ; return#__atstmplab55
    __atstmplab54()
    return
  def __atstmplab54():
    nonlocal env0, env1, arg0, arg1
    nonlocal apy0, apy1, tmp268, tmp269, tmp270, tmp271, tmp272, tmp323
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1, mbranch_2
    tmplab_py = 0
    tmp272 = theWorm_move_right()
    return
  def __atstmplab55():
    nonlocal env0, env1, arg0, arg1
    nonlocal apy0, apy1, tmp268, tmp269, tmp270, tmp271, tmp272, tmp323
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1, mbranch_2
    tmplab_py = 0
    __atstmplab56()
    return
  def __atstmplab56():
    nonlocal env0, env1, arg0, arg1
    nonlocal apy0, apy1, tmp268, tmp269, tmp270, tmp271, tmp272, tmp323
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1, mbranch_2
    tmplab_py = 0
    tmp272 = theWorm_move_search()
    return
  mbranch_1 = { 1: __atstmplab40, 2: __atstmplab41, 3: __atstmplab42 }
  mbranch_2 = { 1: __atstmplab43, 2: __atstmplab44, 3: __atstmplab45, 4: __atstmplab46, 5: __atstmplab47, 6: __atstmplab48, 7: __atstmplab49, 8: __atstmplab50, 9: __atstmplab51, 10: __atstmplab52, 11: __atstmplab53, 12: __atstmplab54, 13: __atstmplab55, 14: __atstmplab56 }
  while(1):
    funlab_py = 0
    #__patsflab_Wormlike__loop_53
    ATSPMVlazyval_eval(arg1); tmp268 = arg1[1]
    if(ATSCKptrisnull(tmp268)): ATSINScaseof_fail("/home/hwxi/Teaching/CS320/cs320-2016-summer-hwxi/assigns/6/Wormlike/MySolution/Wormlike_main.dats: 7624(line=526, offs=3) -- 7656(line=527, offs=21)");
    tmp269 = tmp268[0]
    tmp270 = tmp268[1]
    #ATScaseofseq_beg
    tmplab_py = 1
    while(1):
      mbranch_1.get(tmplab_py)()
      if (tmplab_py == 0): break
    #ATScaseofseq_end
    #ATScaseofseq_beg
    tmplab_py = 1
    while(1):
      mbranch_2.get(tmplab_py)()
      if (tmplab_py == 0): break
    #ATScaseofseq_end
    theGamebd_display__46__1(env0, env1)
    tmp323 = ats2pypre_gte_int0_int0(tmp272, 0)
    if (tmp323):
      #ATStailcalseq_beg
      apy0 = tmp271
      apy1 = tmp270
      arg0 = apy0
      arg1 = apy1
      funlab_py = 1 #__patsflab_Wormlike__loop_53
      #ATStailcalseq_end
    else:
      None#ATSINSmove_void
    #endif
    if (funlab_py == 0): break
  return#_void


def theGamebd_display__46__1(env0, env1):
  tmp205__1 = None
  tmp206__1 = None
  tmp207__1 = None
  tmp208__1 = None
  tmp209__1 = None
  funlab_py = None
  tmplab_py = None
  #__patsflab_theGamebd_display
  tmp205__1 = _057_home_057_hwxi_057_Teaching_057_CS320_057_cs320_055_2016_055_summer_055_hwxi_057_assigns_057_6_057_Wormlike_057_MySolution_057_Wormlike_main_056_dats__x0_get__42__1()
  tmp206__1 = _057_home_057_hwxi_057_Teaching_057_CS320_057_cs320_055_2016_055_summer_055_hwxi_057_assigns_057_6_057_Wormlike_057_MySolution_057_Wormlike_main_056_dats__y0_get__43__1()
  tmp207__1 = _057_home_057_hwxi_057_Teaching_057_CS320_057_cs320_055_2016_055_summer_055_hwxi_057_assigns_057_6_057_Wormlike_057_MySolution_057_Wormlike_main_056_dats__xunit_get__44__1()
  tmp208__1 = _057_home_057_hwxi_057_Teaching_057_CS320_057_cs320_055_2016_055_summer_055_hwxi_057_assigns_057_6_057_Wormlike_057_MySolution_057_Wormlike_main_056_dats__yunit_get__45__1()
  tmp209__1 = theGamebd_get()
  Wormlike__loop1_47__47__1(tmp205__1, tmp206__1, tmp207__1, tmp208__1, tmp209__1, env1, 0)
  _057_home_057_hwxi_057_Teaching_057_CS320_057_cs320_055_2016_055_summer_055_hwxi_057_assigns_057_6_057_Wormlike_057_MySolution_057_Wormlike_main_056_dats__theGamebd_display_end__52__1(env0, env1)
  return#_void


def _057_home_057_hwxi_057_Teaching_057_CS320_057_cs320_055_2016_055_summer_055_hwxi_057_assigns_057_6_057_Wormlike_057_MySolution_057_Wormlike_main_056_dats__x0_get__42__1():
  tmpret200__1 = None
  funlab_py = None
  tmplab_py = None
  #__patsflab_x0_get
  tmpret200__1 = 10
  return tmpret200__1


def _057_home_057_hwxi_057_Teaching_057_CS320_057_cs320_055_2016_055_summer_055_hwxi_057_assigns_057_6_057_Wormlike_057_MySolution_057_Wormlike_main_056_dats__y0_get__43__1():
  tmpret201__1 = None
  funlab_py = None
  tmplab_py = None
  #__patsflab_y0_get
  tmpret201__1 = 10
  return tmpret201__1


def _057_home_057_hwxi_057_Teaching_057_CS320_057_cs320_055_2016_055_summer_055_hwxi_057_assigns_057_6_057_Wormlike_057_MySolution_057_Wormlike_main_056_dats__xunit_get__44__1():
  tmpret202__1 = None
  funlab_py = None
  tmplab_py = None
  #__patsflab_xunit_get
  tmpret202__1 = 20
  return tmpret202__1


def _057_home_057_hwxi_057_Teaching_057_CS320_057_cs320_055_2016_055_summer_055_hwxi_057_assigns_057_6_057_Wormlike_057_MySolution_057_Wormlike_main_056_dats__yunit_get__45__1():
  tmpret203__1 = None
  funlab_py = None
  tmplab_py = None
  #__patsflab_yunit_get
  tmpret203__1 = 20
  return tmpret203__1


def Wormlike__loop1_47__47__1(env0, env1, env2, env3, env4, env5, arg0):
  tmp211__1 = None
  funlab_py = None
  tmplab_py = None
  #__patsflab_Wormlike__loop1_47
  tmp211__1 = ats2pypre_lt_int0_int0(arg0, 32)
  if (tmp211__1):
    Wormlike__loop2_48__48__1(env0, env1, env2, env3, env4, env5, arg0, 0)
  else:
    None#ATSINSmove_void
  #endif
  return#_void


def Wormlike__loop2_48__48__1(env0, env1, env2, env3, env4, env5, arg0, arg1):
  apy0 = None
  apy1 = None
  tmp213__1 = None
  tmp215__1 = None
  tmp216__1 = None
  tmp217__1 = None
  tmp218__1 = None
  tmp219__1 = None
  tmp220__1 = None
  tmp221__1 = None
  funlab_py = None
  tmplab_py = None
  while(1):
    funlab_py = 0
    #__patsflab_Wormlike__loop2_48
    tmp213__1 = ats2pypre_lt_int0_int0(arg1, 32)
    if (tmp213__1):
      tmp215__1 = ats2pypre_mtrxszref_get_at(env4, arg0, arg1)
      tmp217__1 = ats2pypre_mul_int0_int0(arg0, env2)
      tmp216__1 = ats2pypre_add_int0_int0(env0, tmp217__1)
      tmp219__1 = ats2pypre_mul_int0_int0(arg1, env3)
      tmp218__1 = ats2pypre_add_int0_int0(env1, tmp219__1)
      _057_home_057_hwxi_057_Teaching_057_CS320_057_cs320_055_2016_055_summer_055_hwxi_057_assigns_057_6_057_Wormlike_057_MySolution_057_Wormlike_main_056_dats__mydraw_square__51__1(env5, tmp215__1, tmp216__1, tmp218__1, env2, env3)
      tmp220__1 = ats2pypre_add_int0_int0(arg1, 1)
      #ATStailcalseq_beg
      apy0 = arg0
      apy1 = tmp220__1
      arg0 = apy0
      arg1 = apy1
      funlab_py = 1 #__patsflab_Wormlike__loop2_48
      #ATStailcalseq_end
    else:
      tmp221__1 = ats2pypre_add_int0_int0(arg0, 1)
      Wormlike__loop1_47__47__1(env0, env1, env2, env3, env4, env5, tmp221__1)
    #endif
    if (funlab_py == 0): break
  return#_void


def _057_home_057_hwxi_057_Teaching_057_CS320_057_cs320_055_2016_055_summer_055_hwxi_057_assigns_057_6_057_Wormlike_057_MySolution_057_Wormlike_main_056_dats__mydraw_square__51__1(env0, arg0, arg1, arg2, arg3, arg4):
  tmp242__1 = None
  tmp243__1 = None
  tmp244__1 = None
  funlab_py = None
  tmplab_py = None
  #__patsflab_mydraw_square
  tmp242__1 = Wormlike__xnode2color_49(arg0)
  tmp244__1 = ats2py_pygame_rect_make_int4(arg1, arg2, arg3, arg4)
  tmp243__1 = ats2py_pygame_draw_rect_(env0, tmp242__1, tmp244__1)
  None#ATSINSmove_void
  return#_void


def _057_home_057_hwxi_057_Teaching_057_CS320_057_cs320_055_2016_055_summer_055_hwxi_057_assigns_057_6_057_Wormlike_057_MySolution_057_Wormlike_main_056_dats__theGamebd_display_end__52__1(env0, env1):
  tmp246__1 = None
  tmp247__1 = None
  tmp248__1 = None
  tmp249__1 = None
  tmp250__1 = None
  tmp252__1 = None
  tmp253__1 = None
  tmp254__1 = None
  tmp255__1 = None
  tmp256__1 = None
  tmp257__1 = None
  tmp258__1 = None
  tmp259__1 = None
  tmp260__1 = None
  tmp261__1 = None
  tmp262__1 = None
  funlab_py = None
  tmplab_py = None
  mbranch_1 = None
  def __atstmplab36():
    nonlocal env0, env1
    nonlocal tmp246__1, tmp247__1, tmp248__1, tmp249__1, tmp250__1, tmp252__1, tmp253__1, tmp254__1, tmp255__1, tmp256__1, tmp257__1, tmp258__1, tmp259__1, tmp260__1, tmp261__1, tmp262__1
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1
    tmplab_py = 0
    if(ATSCKptriscons(tmp250__1)): tmplab_py = 4 ; return#__atstmplab39
    __atstmplab37()
    return
  def __atstmplab37():
    nonlocal env0, env1
    nonlocal tmp246__1, tmp247__1, tmp248__1, tmp249__1, tmp250__1, tmp252__1, tmp253__1, tmp254__1, tmp255__1, tmp256__1, tmp257__1, tmp258__1, tmp259__1, tmp260__1, tmp261__1, tmp262__1
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1
    tmplab_py = 0
    None#ATSINSmove_void
    return
  def __atstmplab38():
    nonlocal env0, env1
    nonlocal tmp246__1, tmp247__1, tmp248__1, tmp249__1, tmp250__1, tmp252__1, tmp253__1, tmp254__1, tmp255__1, tmp256__1, tmp257__1, tmp258__1, tmp259__1, tmp260__1, tmp261__1, tmp262__1
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1
    tmplab_py = 0
    __atstmplab39()
    return
  def __atstmplab39():
    nonlocal env0, env1
    nonlocal tmp246__1, tmp247__1, tmp248__1, tmp249__1, tmp250__1, tmp252__1, tmp253__1, tmp254__1, tmp255__1, tmp256__1, tmp257__1, tmp258__1, tmp259__1, tmp260__1, tmp261__1, tmp262__1
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1
    tmplab_py = 0
    tmp252__1 = tmp250__1[0]
    tmp253__1 = tmp252__1[0]
    tmp254__1 = tmp252__1[1]
    tmp258__1 = ats2pypre_mul_int0_int0(tmp253__1, tmp248__1)
    tmp257__1 = ats2pypre_add_int0_int0(tmp246__1, tmp258__1)
    tmp260__1 = ats2pypre_mul_int0_int0(tmp254__1, tmp249__1)
    tmp259__1 = ats2pypre_add_int0_int0(tmp247__1, tmp260__1)
    tmp256__1 = ats2py_pygame_rect_make_int4(tmp257__1, tmp259__1, tmp248__1, tmp249__1)
    tmp255__1 = ats2py_pygame_draw_rect_(env1, Wormlike__statmp223, tmp256__1)
    None#ATSINSmove_void
    return
  mbranch_1 = { 1: __atstmplab36, 2: __atstmplab37, 3: __atstmplab38, 4: __atstmplab39 }
  #__patsflab_theGamebd_display_end
  tmp246__1 = _057_home_057_hwxi_057_Teaching_057_CS320_057_cs320_055_2016_055_summer_055_hwxi_057_assigns_057_6_057_Wormlike_057_MySolution_057_Wormlike_main_056_dats__x0_get__42__2()
  tmp247__1 = _057_home_057_hwxi_057_Teaching_057_CS320_057_cs320_055_2016_055_summer_055_hwxi_057_assigns_057_6_057_Wormlike_057_MySolution_057_Wormlike_main_056_dats__y0_get__43__2()
  tmp248__1 = _057_home_057_hwxi_057_Teaching_057_CS320_057_cs320_055_2016_055_summer_055_hwxi_057_assigns_057_6_057_Wormlike_057_MySolution_057_Wormlike_main_056_dats__xunit_get__44__2()
  tmp249__1 = _057_home_057_hwxi_057_Teaching_057_CS320_057_cs320_055_2016_055_summer_055_hwxi_057_assigns_057_6_057_Wormlike_057_MySolution_057_Wormlike_main_056_dats__yunit_get__45__2()
  tmp250__1 = theWorm_last_opt()
  #ATScaseofseq_beg
  tmplab_py = 1
  while(1):
    mbranch_1.get(tmplab_py)()
    if (tmplab_py == 0): break
  #ATScaseofseq_end
  tmp262__1 = (0, 0)
  tmp261__1 = ats2py_pygame_surface_blit_xy(env0, env1, tmp262__1)
  ats2py_pygame_display_flip()
  return#_void


def _057_home_057_hwxi_057_Teaching_057_CS320_057_cs320_055_2016_055_summer_055_hwxi_057_assigns_057_6_057_Wormlike_057_MySolution_057_Wormlike_main_056_dats__x0_get__42__2():
  tmpret200__2 = None
  funlab_py = None
  tmplab_py = None
  #__patsflab_x0_get
  tmpret200__2 = 10
  return tmpret200__2


def _057_home_057_hwxi_057_Teaching_057_CS320_057_cs320_055_2016_055_summer_055_hwxi_057_assigns_057_6_057_Wormlike_057_MySolution_057_Wormlike_main_056_dats__y0_get__43__2():
  tmpret201__2 = None
  funlab_py = None
  tmplab_py = None
  #__patsflab_y0_get
  tmpret201__2 = 10
  return tmpret201__2


def _057_home_057_hwxi_057_Teaching_057_CS320_057_cs320_055_2016_055_summer_055_hwxi_057_assigns_057_6_057_Wormlike_057_MySolution_057_Wormlike_main_056_dats__xunit_get__44__2():
  tmpret202__2 = None
  funlab_py = None
  tmplab_py = None
  #__patsflab_xunit_get
  tmpret202__2 = 20
  return tmpret202__2


def _057_home_057_hwxi_057_Teaching_057_CS320_057_cs320_055_2016_055_summer_055_hwxi_057_assigns_057_6_057_Wormlike_057_MySolution_057_Wormlike_main_056_dats__yunit_get__45__2():
  tmpret203__2 = None
  funlab_py = None
  tmplab_py = None
  #__patsflab_yunit_get
  tmpret203__2 = 20
  return tmpret203__2


def theWormlike_scene1():
  tmp328 = None
  funlab_py = None
  tmplab_py = None
  #__patsflab_theWormlike_scene1
  tmp328 = theGamebd_get()
  ats2pypre_mtrxszref_foreach_cloref(tmp328, Wormlike__patsfun_68__closurerize(tmp328))
  ats2pypre_mtrxszref_foreach_cloref(tmp328, Wormlike__patsfun_69__closurerize(tmp328))
  return#_void


def Wormlike__patsfun_68(env0, arg0, arg1):
  tmp331 = None
  tmp332 = None
  tmp333 = None
  tmp334 = None
  tmp335 = None
  tmp336 = None
  tmp337 = None
  tmp338 = None
  funlab_py = None
  tmplab_py = None
  #__patsflab_Wormlike__patsfun_68
  tmp331 = ats2pypre_eq_int0_int0(arg0, arg1)
  if (tmp331):
    tmp333 = ats2pypre_div_int1_int1(32, 8)
    tmp332 = ats2pypre_gte_int0_int0(arg0, tmp333)
    if (tmp332):
      tmp336 = ats2pypre_mul_int1_int1(7, 32)
      tmp335 = ats2pypre_div_int1_int1(tmp336, 8)
      tmp334 = ats2pypre_lt_int0_int0(arg0, tmp335)
      if (tmp334):
        tmp338 = ats2pypre_neg_int0(1)
        tmp337 = (tmp338, )
        ats2pypre_mtrxszref_set_at(env0, arg0, arg1, tmp337)
      else:
        None#ATSINSmove_void
      #endif
    else:
      None#ATSINSmove_void
    #endif
  else:
    None#ATSINSmove_void
  #endif
  return#_void


def Wormlike__patsfun_69(env0, arg0, arg1):
  tmp340 = None
  tmp341 = None
  tmp342 = None
  tmp343 = None
  tmp344 = None
  tmp345 = None
  tmp346 = None
  tmp347 = None
  tmp348 = None
  tmp349 = None
  funlab_py = None
  tmplab_py = None
  #__patsflab_Wormlike__patsfun_69
  tmp340 = ats2pypre_eq_int0_int0(arg0, arg1)
  if (tmp340):
    tmp342 = ats2pypre_div_int1_int1(32, 8)
    tmp341 = ats2pypre_gte_int0_int0(arg0, tmp342)
    if (tmp341):
      tmp345 = ats2pypre_mul_int1_int1(7, 32)
      tmp344 = ats2pypre_div_int1_int1(tmp345, 8)
      tmp343 = ats2pypre_lt_int0_int0(arg0, tmp344)
      if (tmp343):
        tmp347 = ats2pypre_sub_int1_int1(32, 1)
        tmp346 = ats2pypre_sub_int0_int0(tmp347, arg1)
        tmp349 = ats2pypre_neg_int0(1)
        tmp348 = (tmp349, )
        ats2pypre_mtrxszref_set_at(env0, arg0, tmp346, tmp348)
      else:
        None#ATSINSmove_void
      #endif
    else:
      None#ATSINSmove_void
    #endif
  else:
    None#ATSINSmove_void
  #endif
  return#_void


def theWormlike_scene2():
  tmp351 = None
  funlab_py = None
  tmplab_py = None
  #__patsflab_theWormlike_scene2
  tmp351 = theGamebd_get()
  ats2pypre_mtrxszref_foreach_cloref(tmp351, Wormlike__patsfun_71__closurerize(tmp351))
  return#_void


def Wormlike__patsfun_71(env0, arg0, arg1):
  tmp353 = None
  tmp354 = None
  tmp355 = None
  tmp356 = None
  tmp357 = None
  tmp358 = None
  tmp359 = None
  tmp360 = None
  tmp361 = None
  tmp362 = None
  tmp363 = None
  tmp364 = None
  funlab_py = None
  tmplab_py = None
  #__patsflab_Wormlike__patsfun_71
  tmp354 = ats2pypre_div_int1_int1(32, 8)
  tmp353 = ats2pypre_gte_int0_int0(arg0, tmp354)
  if (tmp353):
    tmp357 = ats2pypre_mul_int1_int1(7, 32)
    tmp356 = ats2pypre_div_int1_int1(tmp357, 8)
    tmp355 = ats2pypre_lt_int0_int0(arg0, tmp356)
    if (tmp355):
      tmp359 = ats2pypre_div_int1_int1(32, 8)
      tmp358 = ats2pypre_gte_int0_int0(arg1, tmp359)
      if (tmp358):
        tmp362 = ats2pypre_mul_int1_int1(7, 32)
        tmp361 = ats2pypre_div_int1_int1(tmp362, 8)
        tmp360 = ats2pypre_lt_int0_int0(arg1, tmp361)
        if (tmp360):
          tmp364 = ats2pypre_neg_int0(1)
          tmp363 = (tmp364, )
          ats2pypre_mtrxszref_set_at(env0, arg0, arg1, tmp363)
        else:
          None#ATSINSmove_void
        #endif
      else:
        None#ATSINSmove_void
      #endif
    else:
      None#ATSINSmove_void
    #endif
  else:
    None#ATSINSmove_void
  #endif
  return#_void


def theWormlike_bonus_rand(arg0):
  tmp366 = None
  tmp380 = None
  funlab_py = None
  tmplab_py = None
  #__patsflab_theWormlike_bonus_rand
  tmp366 = theGamebd_get()
  tmp380 = ats2pypre_int_foreach_method(arg0)
  tmp380[0](tmp380, Wormlike__patsfun_76__closurerize(tmp366))
  return#_void


def Wormlike__I_73():
  tmpret367 = None
  tmp368 = None
  tmp369 = None
  tmp370 = None
  funlab_py = None
  tmplab_py = None
  #__patsflab_Wormlike__I_73
  tmp370 = ats2pylibc_random_random()
  tmp369 = ats2pypre_mul_double_double(tmp370, 0.999999)
  tmp368 = ats2pypre_mul_double_int(tmp369, 32)
  tmpret367 = ats2pypre_double2int(tmp368)
  return tmpret367


def Wormlike__J_74():
  tmpret371 = None
  tmp372 = None
  tmp373 = None
  tmp374 = None
  funlab_py = None
  tmplab_py = None
  #__patsflab_Wormlike__J_74
  tmp374 = ats2pylibc_random_random()
  tmp373 = ats2pypre_mul_double_double(tmp374, 0.999999)
  tmp372 = ats2pypre_mul_double_int(tmp373, 32)
  tmpret371 = ats2pypre_double2int(tmp372)
  return tmpret371


def Wormlike__aux_75(env0):
  tmp376 = None
  tmp377 = None
  tmp378 = None
  tmp379 = None
  funlab_py = None
  tmplab_py = None
  mbranch_1 = None
  def __atstmplab57():
    nonlocal env0
    nonlocal tmp376, tmp377, tmp378, tmp379
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1
    tmplab_py = 0
    if(ATSCKptrisnull(tmp378)): tmplab_py = 4 ; return#__atstmplab60
    __atstmplab58()
    return
  def __atstmplab58():
    nonlocal env0
    nonlocal tmp376, tmp377, tmp378, tmp379
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1
    tmplab_py = 0
    None#ATSINSmove_void
    return
  def __atstmplab59():
    nonlocal env0
    nonlocal tmp376, tmp377, tmp378, tmp379
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1
    tmplab_py = 0
    __atstmplab60()
    return
  def __atstmplab60():
    nonlocal env0
    nonlocal tmp376, tmp377, tmp378, tmp379
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1
    tmplab_py = 0
    tmp379 = (1, )
    ats2pypre_mtrxszref_set_at(env0, tmp376, tmp377, tmp379)
    return
  mbranch_1 = { 1: __atstmplab57, 2: __atstmplab58, 3: __atstmplab59, 4: __atstmplab60 }
  #__patsflab_Wormlike__aux_75
  tmp376 = Wormlike__I_73()
  tmp377 = Wormlike__J_74()
  tmp378 = ats2pypre_mtrxszref_get_at(env0, tmp376, tmp377)
  #ATScaseofseq_beg
  tmplab_py = 1
  while(1):
    mbranch_1.get(tmplab_py)()
    if (tmplab_py == 0): break
  #ATScaseofseq_end
  return#_void


def Wormlike__patsfun_76(env0, arg0):
  funlab_py = None
  tmplab_py = None
  #__patsflab_Wormlike__patsfun_76
  Wormlike__aux_75(env0)
  return#_void


def Wormlike__wnode_mark_77(arg0):
  tmp383 = None
  tmp384 = None
  tmp385 = None
  funlab_py = None
  tmplab_py = None
  #__patsflab_Wormlike__wnode_mark_77
  tmp383 = arg0[0]
  tmp384 = arg0[1]
  tmp385 = theVisitbd_get()
  ats2pypre_mtrxszref_set_at(tmp385, tmp383, tmp384, 1)
  return#_void


def Wormlike__wnode_marked_78(arg0):
  tmpret386 = None
  tmp387 = None
  tmp388 = None
  tmp389 = None
  tmp390 = None
  funlab_py = None
  tmplab_py = None
  #__patsflab_Wormlike__wnode_marked_78
  tmp387 = arg0[0]
  tmp388 = arg0[1]
  tmp389 = theVisitbd_get()
  tmp390 = ats2pypre_mtrxszref_get_at(tmp389, tmp387, tmp388)
  tmpret386 = ats2pypre_gte_int0_int0(tmp390, 1)
  return tmpret386


def _057_home_057_hwxi_057_Teaching_057_CS320_057_cs320_055_2016_055_summer_055_hwxi_057_assigns_057_6_057_Wormlike_057_MySolution_057_Wormlike_main_056_dats__wnode_get_children(arg0):
  tmpret391 = None
  tmp392 = None
  tmp393 = None
  tmp394 = None
  tmp397 = None
  tmp398 = None
  tmp399 = None
  tmp400 = None
  tmp401 = None
  tmp402 = None
  tmp403 = None
  tmp404 = None
  tmp405 = None
  tmp406 = None
  tmp407 = None
  tmp408 = None
  tmp409 = None
  tmp410 = None
  tmp411 = None
  tmp412 = None
  tmp413 = None
  tmp414 = None
  tmp415 = None
  tmp416 = None
  funlab_py = None
  tmplab_py = None
  #__patsflab_wnode_get_children
  tmp392 = arg0[0]
  tmp393 = arg0[1]
  tmp394 = theGamebd_get()
  tmp397 = None
  tmp398 = _057_home_057_hwxi_057_Teaching_057_CS320_057_cs320_055_2016_055_summer_055_hwxi_057_assigns_057_6_057_Wormlike_057_MySolution_057_Wormlike_056_sats__pred_row(tmp392)
  tmp401 = ats2pypre_mtrxszref_get_at(tmp394, tmp398, tmp393)
  tmp400 = Wormlike__test_80(tmp401)
  if (tmp400):
    tmp402 = (tmp398, tmp393)
    tmp399 = (tmp402, tmp397)
  else:
    tmp399 = tmp397
  #endif
  tmp403 = _057_home_057_hwxi_057_Teaching_057_CS320_057_cs320_055_2016_055_summer_055_hwxi_057_assigns_057_6_057_Wormlike_057_MySolution_057_Wormlike_056_sats__succ_row(tmp392)
  tmp406 = ats2pypre_mtrxszref_get_at(tmp394, tmp403, tmp393)
  tmp405 = Wormlike__test_80(tmp406)
  if (tmp405):
    tmp407 = (tmp403, tmp393)
    tmp404 = (tmp407, tmp399)
  else:
    tmp404 = tmp399
  #endif
  tmp408 = _057_home_057_hwxi_057_Teaching_057_CS320_057_cs320_055_2016_055_summer_055_hwxi_057_assigns_057_6_057_Wormlike_057_MySolution_057_Wormlike_056_sats__pred_col(tmp393)
  tmp411 = ats2pypre_mtrxszref_get_at(tmp394, tmp392, tmp408)
  tmp410 = Wormlike__test_80(tmp411)
  if (tmp410):
    tmp412 = (tmp392, tmp408)
    tmp409 = (tmp412, tmp404)
  else:
    tmp409 = tmp404
  #endif
  tmp413 = _057_home_057_hwxi_057_Teaching_057_CS320_057_cs320_055_2016_055_summer_055_hwxi_057_assigns_057_6_057_Wormlike_057_MySolution_057_Wormlike_056_sats__succ_col(tmp393)
  tmp415 = ats2pypre_mtrxszref_get_at(tmp394, tmp392, tmp413)
  tmp414 = Wormlike__test_80(tmp415)
  if (tmp414):
    tmp416 = (tmp392, tmp413)
    tmpret391 = (tmp416, tmp409)
  else:
    tmpret391 = tmp409
  #endif
  return tmpret391


def Wormlike__test_80(arg0):
  tmpret395 = None
  tmp396 = None
  funlab_py = None
  tmplab_py = None
  mbranch_1 = None
  def __atstmplab61():
    nonlocal arg0
    nonlocal tmpret395, tmp396
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1
    tmplab_py = 0
    if(ATSCKptriscons(arg0)): tmplab_py = 4 ; return#__atstmplab64
    __atstmplab62()
    return
  def __atstmplab62():
    nonlocal arg0
    nonlocal tmpret395, tmp396
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1
    tmplab_py = 0
    tmpret395 = True
    return
  def __atstmplab63():
    nonlocal arg0
    nonlocal tmpret395, tmp396
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1
    tmplab_py = 0
    __atstmplab64()
    return
  def __atstmplab64():
    nonlocal arg0
    nonlocal tmpret395, tmp396
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1
    tmplab_py = 0
    tmp396 = arg0[0]
    tmpret395 = ats2pypre_gte_int0_int0(tmp396, 1)
    return
  mbranch_1 = { 1: __atstmplab61, 2: __atstmplab62, 3: __atstmplab63, 4: __atstmplab64 }
  #__patsflab_Wormlike__test_80
  #ATScaseofseq_beg
  tmplab_py = 1
  while(1):
    mbranch_1.get(tmplab_py)()
    if (tmplab_py == 0): break
  #ATScaseofseq_end
  return tmpret395


def _057_home_057_hwxi_057_Teaching_057_CS320_057_cs320_055_2016_055_summer_055_hwxi_057_assigns_057_6_057_Wormlike_057_MySolution_057_Wormlike_main_056_dats__wnode2_mark(arg0):
  tmp418 = None
  funlab_py = None
  tmplab_py = None
  #__patsflab_wnode2_mark
  if(ATSCKptrisnull(arg0)): ATSINScaseof_fail("/home/hwxi/Teaching/CS320/cs320-2016-summer-hwxi/assigns/6/Wormlike/MySolution/Wormlike_search.dats: 1536(line=100, offs=11) -- 1554(line=100, offs=29)");
  tmp418 = arg0[0]
  Wormlike__wnode_mark_77(tmp418)
  return#_void


def _057_home_057_hwxi_057_Teaching_057_CS320_057_cs320_055_2016_055_summer_055_hwxi_057_assigns_057_6_057_Wormlike_057_MySolution_057_Wormlike_main_056_dats__wnode2_marked(arg0):
  tmpret419 = None
  tmp420 = None
  funlab_py = None
  tmplab_py = None
  #__patsflab_wnode2_marked
  if(ATSCKptrisnull(arg0)): ATSINScaseof_fail("/home/hwxi/Teaching/CS320/cs320-2016-summer-hwxi/assigns/6/Wormlike/MySolution/Wormlike_search.dats: 1663(line=107, offs=11) -- 1681(line=107, offs=29)");
  tmp420 = arg0[0]
  tmpret419 = Wormlike__wnode_marked_78(tmp420)
  return tmpret419


def _057_home_057_hwxi_057_Teaching_057_CS320_057_cs320_055_2016_055_summer_055_hwxi_057_assigns_057_6_057_Wormlike_057_MySolution_057_Wormlike_main_056_dats__wnode2_get_children(arg0):
  tmpret421 = None
  tmp422 = None
  tmp423 = None
  funlab_py = None
  tmplab_py = None
  #__patsflab_wnode2_get_children
  if(ATSCKptrisnull(arg0)): ATSINScaseof_fail("/home/hwxi/Teaching/CS320/cs320-2016-summer-hwxi/assigns/6/Wormlike/MySolution/Wormlike_search.dats: 1843(line=119, offs=5) -- 1861(line=119, offs=23)");
  tmp422 = arg0[0]
  tmp423 = _057_home_057_hwxi_057_Teaching_057_CS320_057_cs320_055_2016_055_summer_055_hwxi_057_assigns_057_6_057_Wormlike_057_MySolution_057_Wormlike_main_056_dats__wnode_get_children(tmp422)
  tmpret421 = _057_home_057_hwxi_057_Teaching_057_CS320_057_cs320_055_2016_055_summer_055_hwxi_057_assigns_057_6_057_Wormlike_057_MySolution_057_mylist_056_dats__mylist_map_cloref__84__1(tmp423, Wormlike__patsfun_92__closurerize(arg0))
  return tmpret421


def _057_home_057_hwxi_057_Teaching_057_CS320_057_cs320_055_2016_055_summer_055_hwxi_057_assigns_057_6_057_Wormlike_057_MySolution_057_mylist_056_dats__mylist_map_cloref__84__1(arg0, arg1):
  tmpret424__1 = None
  tmp427__1 = None
  funlab_py = None
  tmplab_py = None
  #__patsflab_mylist_map_cloref
  tmp427__1 = None
  tmpret424__1 = _057_home_057_hwxi_057_Teaching_057_CS320_057_cs320_055_2016_055_summer_055_hwxi_057_assigns_057_6_057_Wormlike_057_MySolution_057_mylist_056_dats__mylist_foldright_cloref__87__1(arg0, Wormlike__patsfun_85__85__1__closurerize(arg1), tmp427__1)
  return tmpret424__1


def _057_home_057_hwxi_057_Teaching_057_CS320_057_cs320_055_2016_055_summer_055_hwxi_057_assigns_057_6_057_Wormlike_057_MySolution_057_mylist_056_dats__mylist_foldright_cloref__87__1(arg0, arg1, arg2):
  tmpret430__1 = None
  funlab_py = None
  tmplab_py = None
  #__patsflab_mylist_foldright_cloref
  tmpret430__1 = Wormlike__aux_88__88__1(arg1, arg0, arg2)
  return tmpret430__1


def Wormlike__aux_88__88__1(env0, arg0, arg1):
  tmpret431__1 = None
  tmp432__1 = None
  tmp433__1 = None
  tmp434__1 = None
  funlab_py = None
  tmplab_py = None
  mbranch_1 = None
  def __atstmplab65():
    nonlocal env0, arg0, arg1
    nonlocal tmpret431__1, tmp432__1, tmp433__1, tmp434__1
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1
    tmplab_py = 0
    if(ATSCKptriscons(arg0)): tmplab_py = 4 ; return#__atstmplab68
    __atstmplab66()
    return
  def __atstmplab66():
    nonlocal env0, arg0, arg1
    nonlocal tmpret431__1, tmp432__1, tmp433__1, tmp434__1
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1
    tmplab_py = 0
    tmpret431__1 = arg1
    return
  def __atstmplab67():
    nonlocal env0, arg0, arg1
    nonlocal tmpret431__1, tmp432__1, tmp433__1, tmp434__1
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1
    tmplab_py = 0
    __atstmplab68()
    return
  def __atstmplab68():
    nonlocal env0, arg0, arg1
    nonlocal tmpret431__1, tmp432__1, tmp433__1, tmp434__1
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1
    tmplab_py = 0
    tmp432__1 = arg0[0]
    tmp433__1 = arg0[1]
    tmp434__1 = Wormlike__aux_88__88__1(env0, tmp433__1, arg1)
    tmpret431__1 = env0[0](env0, tmp432__1, tmp434__1)
    return
  mbranch_1 = { 1: __atstmplab65, 2: __atstmplab66, 3: __atstmplab67, 4: __atstmplab68 }
  #__patsflab_Wormlike__aux_88
  #ATScaseofseq_beg
  tmplab_py = 1
  while(1):
    mbranch_1.get(tmplab_py)()
    if (tmplab_py == 0): break
  #ATScaseofseq_end
  return tmpret431__1


def Wormlike__patsfun_85__85__1(env0, arg0, arg1):
  tmpret425__1 = None
  tmp426__1 = None
  funlab_py = None
  tmplab_py = None
  #__patsflab_Wormlike__patsfun_85
  tmp426__1 = env0[0](env0, arg0)
  tmpret425__1 = (tmp426__1, arg1)
  return tmpret425__1


def Wormlike__patsfun_92(env0, arg0):
  tmpret442 = None
  funlab_py = None
  tmplab_py = None
  #__patsflab_Wormlike__patsfun_92
  tmpret442 = (arg0, env0)
  return tmpret442


def theWorm_move_search():
  tmpret465 = None
  tmp467 = None
  tmp468 = None
  tmp469 = None
  tmp471 = None
  tmp472 = None
  tmp550 = None
  tmp551 = None
  tmp552 = None
  tmp553 = None
  tmp555 = None
  tmp556 = None
  funlab_py = None
  tmplab_py = None
  mbranch_1 = None
  mbranch_2 = None
  mbranch_3 = None
  def __atstmplab77():
    nonlocal tmpret465, tmp467, tmp468, tmp469, tmp471, tmp472, tmp550, tmp551, tmp552, tmp553, tmp555, tmp556
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1, mbranch_2, mbranch_3
    tmplab_py = 0
    if(ATSCKptriscons(tmp467)): tmplab_py = 4 ; return#__atstmplab80
    __atstmplab78()
    return
  def __atstmplab78():
    nonlocal tmpret465, tmp467, tmp468, tmp469, tmp471, tmp472, tmp550, tmp551, tmp552, tmp553, tmp555, tmp556
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1, mbranch_2, mbranch_3
    tmplab_py = 0
    tmp471 = (0, 0)
    tmp468 = (tmp471, )
    return
  def __atstmplab79():
    nonlocal tmpret465, tmp467, tmp468, tmp469, tmp471, tmp472, tmp550, tmp551, tmp552, tmp553, tmp555, tmp556
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1, mbranch_2, mbranch_3
    tmplab_py = 0
    __atstmplab80()
    return
  def __atstmplab80():
    nonlocal tmpret465, tmp467, tmp468, tmp469, tmp471, tmp472, tmp550, tmp551, tmp552, tmp553, tmp555, tmp556
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1, mbranch_2, mbranch_3
    tmplab_py = 0
    tmp469 = tmp467[0]
    tmp551 = None
    tmp550 = (tmp469, tmp551)
    tmp472 = _057_home_057_hwxi_057_Teaching_057_CS320_057_cs320_055_2016_055_summer_055_hwxi_057_assigns_057_6_057_Wormlike_057_MySolution_057_Wormlike_main_056_dats__bfirst_search2__94__1(tmp550)
    #ATScaseofseq_beg
    tmplab_py = 1
    while(1):
      mbranch_2.get(tmplab_py)()
      if (tmplab_py == 0): break
    #ATScaseofseq_end
    return
  def __atstmplab85():
    nonlocal tmpret465, tmp467, tmp468, tmp469, tmp471, tmp472, tmp550, tmp551, tmp552, tmp553, tmp555, tmp556
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1, mbranch_2, mbranch_3
    tmplab_py = 0
    if(ATSCKptriscons(tmp472)): tmplab_py = 4 ; return#__atstmplab88
    __atstmplab86()
    return
  def __atstmplab86():
    nonlocal tmpret465, tmp467, tmp468, tmp469, tmp471, tmp472, tmp550, tmp551, tmp552, tmp553, tmp555, tmp556
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1, mbranch_2, mbranch_3
    tmplab_py = 0
    tmp468 = None
    return
  def __atstmplab87():
    nonlocal tmpret465, tmp467, tmp468, tmp469, tmp471, tmp472, tmp550, tmp551, tmp552, tmp553, tmp555, tmp556
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1, mbranch_2, mbranch_3
    tmplab_py = 0
    __atstmplab88()
    return
  def __atstmplab88():
    nonlocal tmpret465, tmp467, tmp468, tmp469, tmp471, tmp472, tmp550, tmp551, tmp552, tmp553, tmp555, tmp556
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1, mbranch_2, mbranch_3
    tmplab_py = 0
    tmp552 = tmp472[0]
    tmp553 = ats2pypre_ML_list0_length(tmp552)
    tmp555 = ats2pypre_gte_int1_int1(tmp553, 2)
    ats2pypre_assert_errmsg_bool1(tmp555, "/home/hwxi/Teaching/CS320/cs320-2016-summer-hwxi/assigns/6/Wormlike/MySolution/Wormlike_search.dats: 3598(line=216, offs=18) -- 3615(line=216, offs=35)")
    tmp556 = ats2pypre_sub_int1_int1(tmp553, 2)
    tmp468 = ats2pypre_ML_list0_get_at_opt(tmp552, tmp556)
    return
  def __atstmplab89():
    nonlocal tmpret465, tmp467, tmp468, tmp469, tmp471, tmp472, tmp550, tmp551, tmp552, tmp553, tmp555, tmp556
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1, mbranch_2, mbranch_3
    tmplab_py = 0
    if(ATSCKptriscons(tmp468)): tmplab_py = 4 ; return#__atstmplab92
    __atstmplab90()
    return
  def __atstmplab90():
    nonlocal tmpret465, tmp467, tmp468, tmp469, tmp471, tmp472, tmp550, tmp551, tmp552, tmp553, tmp555, tmp556
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1, mbranch_2, mbranch_3
    tmplab_py = 0
    tmpret465 = theWorm_move_random()
    return
  def __atstmplab91():
    nonlocal tmpret465, tmp467, tmp468, tmp469, tmp471, tmp472, tmp550, tmp551, tmp552, tmp553, tmp555, tmp556
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1, mbranch_2, mbranch_3
    tmplab_py = 0
    __atstmplab92()
    return
  def __atstmplab92():
    nonlocal tmpret465, tmp467, tmp468, tmp469, tmp471, tmp472, tmp550, tmp551, tmp552, tmp553, tmp555, tmp556
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1, mbranch_2, mbranch_3
    tmplab_py = 0
    tmpret465 = _057_home_057_hwxi_057_Teaching_057_CS320_057_cs320_055_2016_055_summer_055_hwxi_057_assigns_057_6_057_Wormlike_057_MySolution_057_Wormlike_main_056_dats__theWorm_move_with(tmp468)
    return
  mbranch_1 = { 1: __atstmplab77, 2: __atstmplab78, 3: __atstmplab79, 4: __atstmplab80 }
  mbranch_2 = { 1: __atstmplab85, 2: __atstmplab86, 3: __atstmplab87, 4: __atstmplab88 }
  mbranch_3 = { 1: __atstmplab89, 2: __atstmplab90, 3: __atstmplab91, 4: __atstmplab92 }
  #__patsflab_theWorm_move_search
  theVisitbd_reset()
  tmp467 = theWorm_get_list()
  #ATScaseofseq_beg
  tmplab_py = 1
  while(1):
    mbranch_1.get(tmplab_py)()
    if (tmplab_py == 0): break
  #ATScaseofseq_end
  #ATScaseofseq_beg
  tmplab_py = 1
  while(1):
    mbranch_3.get(tmplab_py)()
    if (tmplab_py == 0): break
  #ATScaseofseq_end
  return tmpret465


def _057_home_057_hwxi_057_Teaching_057_CS320_057_cs320_055_2016_055_summer_055_hwxi_057_assigns_057_6_057_Wormlike_057_MySolution_057_Wormlike_main_056_dats__bfirst_search2__94__1(arg0):
  tmpret450__1 = None
  tmp451__1 = None
  funlab_py = None
  tmplab_py = None
  #__patsflab_bfirst_search2
  tmp451__1 = _057_home_057_hwxi_057_Teaching_057_CS320_057_cs320_055_2016_055_summer_055_hwxi_057_assigns_057_6_057_Wormlike_057_MySolution_057_myqueue_056_dats__queue_make_nil__4__2()
  _057_home_057_hwxi_057_Teaching_057_CS320_057_cs320_055_2016_055_summer_055_hwxi_057_assigns_057_6_057_Wormlike_057_MySolution_057_myqueue_056_dats__queue_enqueue__17__2(tmp451__1, arg0)
  tmpret450__1 = Wormlike__loop_95__95__1(tmp451__1)
  return tmpret450__1


def _057_home_057_hwxi_057_Teaching_057_CS320_057_cs320_055_2016_055_summer_055_hwxi_057_assigns_057_6_057_Wormlike_057_MySolution_057_myqueue_056_dats__queue_make_nil__4__2():
  tmpret11__2 = None
  tmp12__2 = None
  tmp13__2 = None
  tmp14__2 = None
  tmp15__2 = None
  funlab_py = None
  tmplab_py = None
  #__patsflab_queue_make_nil
  tmp13__2 = None
  tmp12__2 = ats2pypre_ref(tmp13__2)
  tmp15__2 = None
  tmp14__2 = ats2pypre_ref(tmp15__2)
  tmpret11__2 = (tmp12__2, tmp14__2)
  return tmpret11__2


def _057_home_057_hwxi_057_Teaching_057_CS320_057_cs320_055_2016_055_summer_055_hwxi_057_assigns_057_6_057_Wormlike_057_MySolution_057_myqueue_056_dats__queue_enqueue__17__2(arg0, arg1):
  tmp81__2 = None
  tmp82__2 = None
  tmp83__2 = None
  funlab_py = None
  tmplab_py = None
  #__patsflab_queue_enqueue
  tmp81__2 = arg0[0]
  tmp83__2 = ats2pypre_ref_get_elt(tmp81__2)
  tmp82__2 = (arg1, tmp83__2)
  ats2pypre_ref_set_elt(tmp81__2, tmp82__2)
  return#_void


def Wormlike__loop_95__95__1(env0):
  tmpret453__1 = None
  tmp454__1 = None
  tmp455__1 = None
  tmp456__1 = None
  tmp457__1 = None
  tmp458__1 = None
  funlab_py = None
  tmplab_py = None
  mbranch_1 = None
  def __atstmplab73():
    nonlocal env0
    nonlocal tmpret453__1, tmp454__1, tmp455__1, tmp456__1, tmp457__1, tmp458__1
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1
    tmplab_py = 0
    if(ATSCKptriscons(tmp454__1)): tmplab_py = 4 ; return#__atstmplab76
    __atstmplab74()
    return
  def __atstmplab74():
    nonlocal env0
    nonlocal tmpret453__1, tmp454__1, tmp455__1, tmp456__1, tmp457__1, tmp458__1
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1
    tmplab_py = 0
    tmpret453__1 = None
    return
  def __atstmplab75():
    nonlocal env0
    nonlocal tmpret453__1, tmp454__1, tmp455__1, tmp456__1, tmp457__1, tmp458__1
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1
    tmplab_py = 0
    __atstmplab76()
    return
  def __atstmplab76():
    nonlocal env0
    nonlocal tmpret453__1, tmp454__1, tmp455__1, tmp456__1, tmp457__1, tmp458__1
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1
    tmplab_py = 0
    tmp455__1 = tmp454__1[0]
    tmp456__1 = _057_home_057_hwxi_057_Teaching_057_CS320_057_cs320_055_2016_055_summer_055_hwxi_057_assigns_057_6_057_Wormlike_057_MySolution_057_Wormlike_main_056_dats__process2__93__1(tmp455__1)
    if (tmp456__1):
      tmpret453__1 = (tmp455__1, )
    else:
      tmp457__1 = _057_home_057_hwxi_057_Teaching_057_CS320_057_cs320_055_2016_055_summer_055_hwxi_057_assigns_057_6_057_Wormlike_057_MySolution_057_Wormlike_main_056_dats__wnode2_get_children(tmp455__1)
      tmp458__1 = _057_home_057_hwxi_057_Teaching_057_CS320_057_cs320_055_2016_055_summer_055_hwxi_057_assigns_057_6_057_Wormlike_057_MySolution_057_mylist_056_dats__mylist_filter_cloref__106__1(tmp457__1, Wormlike__patsfun_96__96__1__closurerize())
      _057_home_057_hwxi_057_Teaching_057_CS320_057_cs320_055_2016_055_summer_055_hwxi_057_assigns_057_6_057_Wormlike_057_MySolution_057_mylist_056_dats__mylist_foreach_cloref__113__1(tmp458__1, Wormlike__patsfun_97__97__1__closurerize())
      _057_home_057_hwxi_057_Teaching_057_CS320_057_cs320_055_2016_055_summer_055_hwxi_057_assigns_057_6_057_Wormlike_057_MySolution_057_mylist_056_dats__mylist_foreach_cloref__113__2(tmp458__1, Wormlike__patsfun_98__98__1__closurerize(env0))
      #ATStailcalseq_beg
      funlab_py = 1 #__patsflab_Wormlike__loop_95
      #ATStailcalseq_end
    #endif
    return
  mbranch_1 = { 1: __atstmplab73, 2: __atstmplab74, 3: __atstmplab75, 4: __atstmplab76 }
  while(1):
    funlab_py = 0
    #__patsflab_Wormlike__loop_95
    tmp454__1 = _057_home_057_hwxi_057_Teaching_057_CS320_057_cs320_055_2016_055_summer_055_hwxi_057_assigns_057_6_057_Wormlike_057_MySolution_057_myqueue_056_dats__queue_dequeue_opt__14__2(env0)
    #ATScaseofseq_beg
    tmplab_py = 1
    while(1):
      mbranch_1.get(tmplab_py)()
      if (tmplab_py == 0): break
    #ATScaseofseq_end
    if (funlab_py == 0): break
  return tmpret453__1


def _057_home_057_hwxi_057_Teaching_057_CS320_057_cs320_055_2016_055_summer_055_hwxi_057_assigns_057_6_057_Wormlike_057_MySolution_057_myqueue_056_dats__queue_dequeue_opt__14__2(arg0):
  tmpret51__2 = None
  tmp52__2 = None
  tmp53__2 = None
  tmp54__2 = None
  tmp55__2 = None
  tmp56__2 = None
  tmp57__2 = None
  tmp58__2 = None
  tmp60__2 = None
  tmp61__2 = None
  tmp62__2 = None
  funlab_py = None
  tmplab_py = None
  mbranch_1 = None
  mbranch_2 = None
  def __atstmplab0():
    nonlocal arg0
    nonlocal tmpret51__2, tmp52__2, tmp53__2, tmp54__2, tmp55__2, tmp56__2, tmp57__2, tmp58__2, tmp60__2, tmp61__2, tmp62__2
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1, mbranch_2
    tmplab_py = 0
    if(ATSCKptriscons(tmp53__2)): tmplab_py = 4 ; return#__atstmplab3
    __atstmplab1()
    return
  def __atstmplab1():
    nonlocal arg0
    nonlocal tmpret51__2, tmp52__2, tmp53__2, tmp54__2, tmp55__2, tmp56__2, tmp57__2, tmp58__2, tmp60__2, tmp61__2, tmp62__2
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1, mbranch_2
    tmplab_py = 0
    tmp56__2 = arg0[0]
    tmp58__2 = ats2pypre_ref_get_elt(tmp56__2)
    tmp57__2 = ats2pypre_ML_list0_reverse(tmp58__2)
    tmp60__2 = None
    ats2pypre_ref_set_elt(tmp56__2, tmp60__2)
    #ATScaseofseq_beg
    tmplab_py = 1
    while(1):
      mbranch_2.get(tmplab_py)()
      if (tmplab_py == 0): break
    #ATScaseofseq_end
    return
  def __atstmplab2():
    nonlocal arg0
    nonlocal tmpret51__2, tmp52__2, tmp53__2, tmp54__2, tmp55__2, tmp56__2, tmp57__2, tmp58__2, tmp60__2, tmp61__2, tmp62__2
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1, mbranch_2
    tmplab_py = 0
    if(ATSCKptrisnull(tmp53__2)): ATSINScaseof_fail("/home/hwxi/Teaching/CS320/cs320-2016-summer-hwxi/assigns/6/Wormlike/MySolution/myqueue.dats: 1728(line=126, offs=3) -- 2010(line=136, offs=42)");
    __atstmplab3()
    return
  def __atstmplab3():
    nonlocal arg0
    nonlocal tmpret51__2, tmp52__2, tmp53__2, tmp54__2, tmp55__2, tmp56__2, tmp57__2, tmp58__2, tmp60__2, tmp61__2, tmp62__2
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1, mbranch_2
    tmplab_py = 0
    tmp54__2 = tmp53__2[0]
    tmp55__2 = tmp53__2[1]
    ats2pypre_ref_set_elt(tmp52__2, tmp55__2)
    tmpret51__2 = (tmp54__2, )
    return
  def __atstmplab4():
    nonlocal arg0
    nonlocal tmpret51__2, tmp52__2, tmp53__2, tmp54__2, tmp55__2, tmp56__2, tmp57__2, tmp58__2, tmp60__2, tmp61__2, tmp62__2
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1, mbranch_2
    tmplab_py = 0
    if(ATSCKptriscons(tmp57__2)): tmplab_py = 4 ; return#__atstmplab7
    __atstmplab5()
    return
  def __atstmplab5():
    nonlocal arg0
    nonlocal tmpret51__2, tmp52__2, tmp53__2, tmp54__2, tmp55__2, tmp56__2, tmp57__2, tmp58__2, tmp60__2, tmp61__2, tmp62__2
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1, mbranch_2
    tmplab_py = 0
    tmpret51__2 = None
    return
  def __atstmplab6():
    nonlocal arg0
    nonlocal tmpret51__2, tmp52__2, tmp53__2, tmp54__2, tmp55__2, tmp56__2, tmp57__2, tmp58__2, tmp60__2, tmp61__2, tmp62__2
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1, mbranch_2
    tmplab_py = 0
    __atstmplab7()
    return
  def __atstmplab7():
    nonlocal arg0
    nonlocal tmpret51__2, tmp52__2, tmp53__2, tmp54__2, tmp55__2, tmp56__2, tmp57__2, tmp58__2, tmp60__2, tmp61__2, tmp62__2
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1, mbranch_2
    tmplab_py = 0
    tmp61__2 = tmp57__2[0]
    tmp62__2 = tmp57__2[1]
    ats2pypre_ref_set_elt(tmp52__2, tmp62__2)
    tmpret51__2 = (tmp61__2, )
    return
  mbranch_1 = { 1: __atstmplab0, 2: __atstmplab1, 3: __atstmplab2, 4: __atstmplab3 }
  mbranch_2 = { 1: __atstmplab4, 2: __atstmplab5, 3: __atstmplab6, 4: __atstmplab7 }
  #__patsflab_queue_dequeue_opt
  tmp52__2 = arg0[1]
  tmp53__2 = ats2pypre_ref_get_elt(tmp52__2)
  #ATScaseofseq_beg
  tmplab_py = 1
  while(1):
    mbranch_1.get(tmplab_py)()
    if (tmplab_py == 0): break
  #ATScaseofseq_end
  return tmpret51__2


def _057_home_057_hwxi_057_Teaching_057_CS320_057_cs320_055_2016_055_summer_055_hwxi_057_assigns_057_6_057_Wormlike_057_MySolution_057_Wormlike_main_056_dats__process2__93__1(arg0):
  tmpret443__1 = None
  tmp444__1 = None
  tmp445__1 = None
  tmp446__1 = None
  tmp447__1 = None
  tmp448__1 = None
  tmp449__1 = None
  funlab_py = None
  tmplab_py = None
  mbranch_1 = None
  def __atstmplab69():
    nonlocal arg0
    nonlocal tmpret443__1, tmp444__1, tmp445__1, tmp446__1, tmp447__1, tmp448__1, tmp449__1
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1
    tmplab_py = 0
    if(ATSCKptriscons(tmp448__1)): tmplab_py = 4 ; return#__atstmplab72
    __atstmplab70()
    return
  def __atstmplab70():
    nonlocal arg0
    nonlocal tmpret443__1, tmp444__1, tmp445__1, tmp446__1, tmp447__1, tmp448__1, tmp449__1
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1
    tmplab_py = 0
    tmpret443__1 = False
    return
  def __atstmplab71():
    nonlocal arg0
    nonlocal tmpret443__1, tmp444__1, tmp445__1, tmp446__1, tmp447__1, tmp448__1, tmp449__1
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1
    tmplab_py = 0
    __atstmplab72()
    return
  def __atstmplab72():
    nonlocal arg0
    nonlocal tmpret443__1, tmp444__1, tmp445__1, tmp446__1, tmp447__1, tmp448__1, tmp449__1
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1
    tmplab_py = 0
    tmp449__1 = tmp448__1[0]
    tmpret443__1 = ats2pypre_gte_int0_int0(tmp449__1, 1)
    return
  mbranch_1 = { 1: __atstmplab69, 2: __atstmplab70, 3: __atstmplab71, 4: __atstmplab72 }
  #__patsflab_process2
  if(ATSCKptrisnull(arg0)): ATSINScaseof_fail("/home/hwxi/Teaching/CS320/cs320-2016-summer-hwxi/assigns/6/Wormlike/MySolution/Wormlike_search.dats: 2060(line=132, offs=5) -- 2083(line=132, offs=28)");
  tmp444__1 = arg0[0]
  tmp445__1 = tmp444__1[0]
  tmp446__1 = tmp444__1[1]
  tmp447__1 = theGamebd_get()
  tmp448__1 = ats2pypre_mtrxszref_get_at(tmp447__1, tmp445__1, tmp446__1)
  #ATScaseofseq_beg
  tmplab_py = 1
  while(1):
    mbranch_1.get(tmplab_py)()
    if (tmplab_py == 0): break
  #ATScaseofseq_end
  return tmpret443__1


def _057_home_057_hwxi_057_Teaching_057_CS320_057_cs320_055_2016_055_summer_055_hwxi_057_assigns_057_6_057_Wormlike_057_MySolution_057_mylist_056_dats__mylist_filter_cloref__106__1(arg0, arg1):
  tmpret514__1 = None
  tmp517__1 = None
  funlab_py = None
  tmplab_py = None
  #__patsflab_mylist_filter_cloref
  tmp517__1 = None
  tmpret514__1 = _057_home_057_hwxi_057_Teaching_057_CS320_057_cs320_055_2016_055_summer_055_hwxi_057_assigns_057_6_057_Wormlike_057_MySolution_057_mylist_056_dats__mylist_foldright_cloref__87__2(arg0, Wormlike__patsfun_107__107__1__closurerize(arg1), tmp517__1)
  return tmpret514__1


def _057_home_057_hwxi_057_Teaching_057_CS320_057_cs320_055_2016_055_summer_055_hwxi_057_assigns_057_6_057_Wormlike_057_MySolution_057_mylist_056_dats__mylist_foldright_cloref__87__2(arg0, arg1, arg2):
  tmpret430__2 = None
  funlab_py = None
  tmplab_py = None
  #__patsflab_mylist_foldright_cloref
  tmpret430__2 = Wormlike__aux_88__88__2(arg1, arg0, arg2)
  return tmpret430__2


def Wormlike__aux_88__88__2(env0, arg0, arg1):
  tmpret431__2 = None
  tmp432__2 = None
  tmp433__2 = None
  tmp434__2 = None
  funlab_py = None
  tmplab_py = None
  mbranch_1 = None
  def __atstmplab65():
    nonlocal env0, arg0, arg1
    nonlocal tmpret431__2, tmp432__2, tmp433__2, tmp434__2
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1
    tmplab_py = 0
    if(ATSCKptriscons(arg0)): tmplab_py = 4 ; return#__atstmplab68
    __atstmplab66()
    return
  def __atstmplab66():
    nonlocal env0, arg0, arg1
    nonlocal tmpret431__2, tmp432__2, tmp433__2, tmp434__2
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1
    tmplab_py = 0
    tmpret431__2 = arg1
    return
  def __atstmplab67():
    nonlocal env0, arg0, arg1
    nonlocal tmpret431__2, tmp432__2, tmp433__2, tmp434__2
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1
    tmplab_py = 0
    __atstmplab68()
    return
  def __atstmplab68():
    nonlocal env0, arg0, arg1
    nonlocal tmpret431__2, tmp432__2, tmp433__2, tmp434__2
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1
    tmplab_py = 0
    tmp432__2 = arg0[0]
    tmp433__2 = arg0[1]
    tmp434__2 = Wormlike__aux_88__88__2(env0, tmp433__2, arg1)
    tmpret431__2 = env0[0](env0, tmp432__2, tmp434__2)
    return
  mbranch_1 = { 1: __atstmplab65, 2: __atstmplab66, 3: __atstmplab67, 4: __atstmplab68 }
  #__patsflab_Wormlike__aux_88
  #ATScaseofseq_beg
  tmplab_py = 1
  while(1):
    mbranch_1.get(tmplab_py)()
    if (tmplab_py == 0): break
  #ATScaseofseq_end
  return tmpret431__2


def Wormlike__patsfun_107__107__1(env0, arg0, arg1):
  tmpret515__1 = None
  tmp516__1 = None
  funlab_py = None
  tmplab_py = None
  #__patsflab_Wormlike__patsfun_107
  tmp516__1 = env0[0](env0, arg0)
  if (tmp516__1):
    tmpret515__1 = (arg0, arg1)
  else:
    tmpret515__1 = arg1
  #endif
  return tmpret515__1


def Wormlike__patsfun_96__96__1(arg0):
  tmpret459__1 = None
  tmp460__1 = None
  funlab_py = None
  tmplab_py = None
  #__patsflab_Wormlike__patsfun_96
  tmp460__1 = _057_home_057_hwxi_057_Teaching_057_CS320_057_cs320_055_2016_055_summer_055_hwxi_057_assigns_057_6_057_Wormlike_057_MySolution_057_Wormlike_main_056_dats__wnode2_marked(arg0)
  tmpret459__1 = ats2pypre_not_bool0(tmp460__1)
  return tmpret459__1


def _057_home_057_hwxi_057_Teaching_057_CS320_057_cs320_055_2016_055_summer_055_hwxi_057_assigns_057_6_057_Wormlike_057_MySolution_057_mylist_056_dats__mylist_foreach_cloref__113__1(arg0, arg1):
  funlab_py = None
  tmplab_py = None
  #__patsflab_mylist_foreach_cloref
  Wormlike__loop_114__114__1(arg1, arg0)
  return#_void


def Wormlike__loop_114__114__1(env0, arg0):
  apy0 = None
  tmp531__1 = None
  tmp532__1 = None
  funlab_py = None
  tmplab_py = None
  mbranch_1 = None
  def __atstmplab81():
    nonlocal env0, arg0
    nonlocal apy0, tmp531__1, tmp532__1
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1
    tmplab_py = 0
    if(ATSCKptriscons(arg0)): tmplab_py = 4 ; return#__atstmplab84
    __atstmplab82()
    return
  def __atstmplab82():
    nonlocal env0, arg0
    nonlocal apy0, tmp531__1, tmp532__1
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1
    tmplab_py = 0
    None#ATSINSmove_void
    return
  def __atstmplab83():
    nonlocal env0, arg0
    nonlocal apy0, tmp531__1, tmp532__1
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1
    tmplab_py = 0
    __atstmplab84()
    return
  def __atstmplab84():
    nonlocal env0, arg0
    nonlocal apy0, tmp531__1, tmp532__1
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1
    tmplab_py = 0
    tmp531__1 = arg0[0]
    tmp532__1 = arg0[1]
    env0[0](env0, tmp531__1)
    #ATStailcalseq_beg
    apy0 = tmp532__1
    arg0 = apy0
    funlab_py = 1 #__patsflab_Wormlike__loop_114
    #ATStailcalseq_end
    return
  mbranch_1 = { 1: __atstmplab81, 2: __atstmplab82, 3: __atstmplab83, 4: __atstmplab84 }
  while(1):
    funlab_py = 0
    #__patsflab_Wormlike__loop_114
    #ATScaseofseq_beg
    tmplab_py = 1
    while(1):
      mbranch_1.get(tmplab_py)()
      if (tmplab_py == 0): break
    #ATScaseofseq_end
    if (funlab_py == 0): break
  return#_void


def Wormlike__patsfun_97__97__1(arg0):
  funlab_py = None
  tmplab_py = None
  #__patsflab_Wormlike__patsfun_97
  _057_home_057_hwxi_057_Teaching_057_CS320_057_cs320_055_2016_055_summer_055_hwxi_057_assigns_057_6_057_Wormlike_057_MySolution_057_Wormlike_main_056_dats__wnode2_mark(arg0)
  return#_void


def _057_home_057_hwxi_057_Teaching_057_CS320_057_cs320_055_2016_055_summer_055_hwxi_057_assigns_057_6_057_Wormlike_057_MySolution_057_mylist_056_dats__mylist_foreach_cloref__113__2(arg0, arg1):
  funlab_py = None
  tmplab_py = None
  #__patsflab_mylist_foreach_cloref
  Wormlike__loop_114__114__2(arg1, arg0)
  return#_void


def Wormlike__loop_114__114__2(env0, arg0):
  apy0 = None
  tmp531__2 = None
  tmp532__2 = None
  funlab_py = None
  tmplab_py = None
  mbranch_1 = None
  def __atstmplab81():
    nonlocal env0, arg0
    nonlocal apy0, tmp531__2, tmp532__2
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1
    tmplab_py = 0
    if(ATSCKptriscons(arg0)): tmplab_py = 4 ; return#__atstmplab84
    __atstmplab82()
    return
  def __atstmplab82():
    nonlocal env0, arg0
    nonlocal apy0, tmp531__2, tmp532__2
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1
    tmplab_py = 0
    None#ATSINSmove_void
    return
  def __atstmplab83():
    nonlocal env0, arg0
    nonlocal apy0, tmp531__2, tmp532__2
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1
    tmplab_py = 0
    __atstmplab84()
    return
  def __atstmplab84():
    nonlocal env0, arg0
    nonlocal apy0, tmp531__2, tmp532__2
    nonlocal funlab_py, tmplab_py
    nonlocal mbranch_1
    tmplab_py = 0
    tmp531__2 = arg0[0]
    tmp532__2 = arg0[1]
    env0[0](env0, tmp531__2)
    #ATStailcalseq_beg
    apy0 = tmp532__2
    arg0 = apy0
    funlab_py = 1 #__patsflab_Wormlike__loop_114
    #ATStailcalseq_end
    return
  mbranch_1 = { 1: __atstmplab81, 2: __atstmplab82, 3: __atstmplab83, 4: __atstmplab84 }
  while(1):
    funlab_py = 0
    #__patsflab_Wormlike__loop_114
    #ATScaseofseq_beg
    tmplab_py = 1
    while(1):
      mbranch_1.get(tmplab_py)()
      if (tmplab_py == 0): break
    #ATScaseofseq_end
    if (funlab_py == 0): break
  return#_void


def Wormlike__patsfun_98__98__1(env0, arg0):
  funlab_py = None
  tmplab_py = None
  #__patsflab_Wormlike__patsfun_98
  _057_home_057_hwxi_057_Teaching_057_CS320_057_cs320_055_2016_055_summer_055_hwxi_057_assigns_057_6_057_Wormlike_057_MySolution_057_myqueue_056_dats__queue_enqueue__17__3(env0, arg0)
  return#_void


def _057_home_057_hwxi_057_Teaching_057_CS320_057_cs320_055_2016_055_summer_055_hwxi_057_assigns_057_6_057_Wormlike_057_MySolution_057_myqueue_056_dats__queue_enqueue__17__3(arg0, arg1):
  tmp81__3 = None
  tmp82__3 = None
  tmp83__3 = None
  funlab_py = None
  tmplab_py = None
  #__patsflab_queue_enqueue
  tmp81__3 = arg0[0]
  tmp83__3 = ats2pypre_ref_get_elt(tmp81__3)
  tmp82__3 = (arg1, tmp83__3)
  ats2pypre_ref_set_elt(tmp81__3, tmp82__3)
  return#_void


def _057_home_057_hwxi_057_Teaching_057_CS320_057_cs320_055_2016_055_summer_055_hwxi_057_assigns_057_6_057_Wormlike_057_MySolution_057_Wormlike_056_sats__kstream_make(arg0):
  tmpret557 = None
  funlab_py = None
  tmplab_py = None
  #__patsflab_kstream_make
  tmpret557 = Wormlike__aux_123(arg0)
  return tmpret557


def Wormlike__aux_123(env0):
  tmpret558 = None
  funlab_py = None
  tmplab_py = None
  #__patsflab_Wormlike__aux_123
  tmpret558 = [0, Wormlike__patsfun_124__closurerize(env0)]
  return tmpret558


def Wormlike__patsfun_124(env0):
  tmpret559 = None
  tmp560 = None
  tmp561 = None
  tmp562 = None
  tmp563 = None
  tmp564 = None
  tmp565 = None
  tmp566 = None
  tmp567 = None
  tmp568 = None
  tmp569 = None
  tmp570 = None
  tmp571 = None
  tmp572 = None
  tmp573 = None
  tmp574 = None
  tmp575 = None
  tmp576 = None
  tmp577 = None
  tmp578 = None
  tmp579 = None
  tmp580 = None
  tmp581 = None
  tmp582 = None
  tmp583 = None
  tmp584 = None
  tmp585 = None
  tmp586 = None
  tmp587 = None
  tmp588 = None
  tmp589 = None
  funlab_py = None
  tmplab_py = None
  #__patsflab_Wormlike__patsfun_124
  tmp560 = ats2py_pygame_time_wait(env0)
  tmp561 = ats2py_pygame_event_poll()
  tmp562 = ats2py_pygame_event_type(tmp561)
  tmp563 = ats2py_pygame_event_type_equal(tmp562, pygame.QUIT)
  if (tmp563):
    tmp564 = 1
    tmp565 = Wormlike__aux_123(env0)
    tmpret559 = (tmp564, tmp565)
  else:
    tmp566 = ats2py_pygame_event_type_equal(tmp562, pygame.NOEVENT)
    if (tmp566):
      tmp567 = 0
      tmp568 = Wormlike__aux_123(env0)
      tmpret559 = (tmp567, tmp568)
    else:
      tmp569 = ats2py_pygame_event_type_equal(tmp562, pygame.KEYDOWN)
      if (tmp569):
        tmp570 = ats2py_pygame_event_keydown_key(tmp561)
        tmp571 = ats2pypre_eq_int0_int0(tmp570, pygame.K_UP)
        if (tmp571):
          tmp572 = 2
          tmp573 = Wormlike__aux_123(env0)
          tmpret559 = (tmp572, tmp573)
        else:
          tmp574 = ats2pypre_eq_int0_int0(tmp570, pygame.K_DOWN)
          if (tmp574):
            tmp575 = 3
            tmp576 = Wormlike__aux_123(env0)
            tmpret559 = (tmp575, tmp576)
          else:
            tmp577 = ats2pypre_eq_int0_int0(tmp570, pygame.K_LEFT)
            if (tmp577):
              tmp578 = 4
              tmp579 = Wormlike__aux_123(env0)
              tmpret559 = (tmp578, tmp579)
            else:
              tmp580 = ats2pypre_eq_int0_int0(tmp570, pygame.K_RIGHT)
              if (tmp580):
                tmp581 = 5
                tmp582 = Wormlike__aux_123(env0)
                tmpret559 = (tmp581, tmp582)
              else:
                tmp583 = ats2pypre_eq_int0_int0(tmp570, pygame.K_SPACE)
                if (tmp583):
                  tmp584 = 6
                  tmp585 = Wormlike__aux_123(env0)
                  tmpret559 = (tmp584, tmp585)
                else:
                  tmp586 = 0
                  tmp587 = Wormlike__aux_123(env0)
                  tmpret559 = (tmp586, tmp587)
                #endif
              #endif
            #endif
          #endif
        #endif
      else:
        tmp588 = 0
        tmp589 = Wormlike__aux_123(env0)
        tmpret559 = (tmp588, tmp589)
      #endif
    #endif
  #endif
  return tmpret559


_057_home_057_hwxi_057_Teaching_057_CS320_057_cs320_055_2016_055_summer_055_hwxi_057_assigns_057_6_057_Wormlike_057_MySolution_057_Wormlike_main_056_dats__dynloadflag = 0

def _057_home_057_hwxi_057_Teaching_057_CS320_057_cs320_055_2016_055_summer_055_hwxi_057_assigns_057_6_057_Wormlike_057_MySolution_057_Wormlike_main_056_dats__dynload():
  funlab_py = None
  tmplab_py = None
  #ATSdynload()
  global Wormlike__statmp10
  global Wormlike__statmp192
  global Wormlike__statmp193
  global Wormlike__statmp195
  global Wormlike__statmp223
  global Wormlike__statmp224
  global Wormlike__statmp225
  global Wormlike__statmp226
  global Wormlike__statmp227
  #ATSdynloadflag_sta
  global _057_home_057_hwxi_057_Teaching_057_CS320_057_cs320_055_2016_055_summer_055_hwxi_057_assigns_057_6_057_Wormlike_057_MySolution_057_Wormlike_main_056_dats__dynloadflag
  if (ATSCKiseqz(_057_home_057_hwxi_057_Teaching_057_CS320_057_cs320_055_2016_055_summer_055_hwxi_057_assigns_057_6_057_Wormlike_057_MySolution_057_Wormlike_main_056_dats__dynloadflag)):
    #ATSdynloadset
    _057_home_057_hwxi_057_Teaching_057_CS320_057_cs320_055_2016_055_summer_055_hwxi_057_assigns_057_6_057_Wormlike_057_MySolution_057_Wormlike_main_056_dats__dynloadflag = 1
    Wormlike__statmp10 = _057_home_057_hwxi_057_Teaching_057_CS320_057_cs320_055_2016_055_summer_055_hwxi_057_assigns_057_6_057_Wormlike_057_MySolution_057_myqueue_056_dats__queue_make_nil__4__1()
    Wormlike__statmp193 = None
    Wormlike__statmp192 = ats2pypre_mtrxszref_make_elt(32, 32, Wormlike__statmp193)
    Wormlike__statmp195 = ats2pypre_mtrxszref_make_elt(32, 32, 0)
    Wormlike__statmp223 = ats2py_pygame_color_make_rgb(255, 0, 0)
    Wormlike__statmp224 = ats2py_pygame_color_make_rgb(200, 200, 200)
    Wormlike__statmp225 = ats2py_pygame_color_make_rgb(0, 0, 0)
    Wormlike__statmp226 = ats2py_pygame_color_make_rgb(255, 255, 255)
    Wormlike__statmp227 = ats2py_pygame_color_make_rgb(255, 255, 0)
  #endif
  return#_void


def Wormlike__dynload():
  funlab_py = None
  tmplab_py = None
  _057_home_057_hwxi_057_Teaching_057_CS320_057_cs320_055_2016_055_summer_055_hwxi_057_assigns_057_6_057_Wormlike_057_MySolution_057_Wormlike_main_056_dats__dynload()
  return#_void


######
#ATSextcode_beg()
######
if __name__ == '__main__':
  Wormlike__dynload(); Wormlike__main()
######
#ATSextcode_end()
######
######
##
## end-of-compilation-unit
##
######
