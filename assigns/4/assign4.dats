//
// Title:
// Concepts of Programming Languages
// Number: CAS CS 320
// Semester: Summer I, 2016
// Class Time: MTWR 3:00-5:00
// Instructor: Hongwei Xi (hwxiATbuDOTedu)
// Teaching Fellow: Hanwen Wu (hwwuATbuDOTedu)
//
(*
//
// Assignment #4
// Due Tuesday, the 21st of June, 2016 at 11:59pm
//
*)
(* ****** ****** *)
//
#include
"share/atspre_staload.hats"
#include
"share/HATS/atspre_staload_libats_ML.hats"
//
(* ****** ****** *)

(*
//
// Points: 10
//
The following is a well-known series:

ln 2 = 1 - 1/2 + 1/3 - 1/4 + ...

Please implement a stream consisting of all the partial sums of this
series. Then compute an accurate approximation to ln2 by using Euler's
transform (only if you do the following bonus question).
//
*)
extern
fun
stream_ln2(): stream(double)

(* ****** ****** *)
//
// Points: 10
//
// Please implement the following function that enumerates
// all the pairs of natural numbers (i, j) satisfying i <= j:
//
typedef int2 = (int, int)
//
extern
fun
intpair_enumerate ((*void*)): stream(int2)
//
(* ****** ****** *)
//
// HX: 20 *bonus* points
// Please implement the Euler transform:
// Given a sequence:
//   x(0), x(1), x(2), ..., x(n), ...
// We can form another sequence:
//   y(0), y(1), y(2), ..., y(n), ....
// such that each y(n) equals the the follow value
//
// x(n+2) - (x(n+2)-x(n+1))^2 / (x(n)+x(n+2)-2*x(n+1))
//
extern
fun
EulerTrans(xs: stream(double)): stream(double)
//
(* ****** ****** *)

(* end of [assign4.dats] *)
