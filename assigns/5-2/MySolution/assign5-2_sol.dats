//
// Title:
// Concepts of Programming Languages
// Number: CAS CS 320
// Semester: Summer I, 2016
// Class Time: MTWR 3:00-5:00
// Instructor: Hongwei Xi (hwxiATbuDOTedu)
// Teaching Fellow: Hanwen Wu (hwwuATbuDOTedu)
//
(*
//
// Assignment #5-2
// Due Tuesday, the 28th of June, 2016 at 11:59pm
//
*)
(* ****** ****** *)
//
#include "./../doublet.dats"
//
(* ****** ****** *)

(*
**
** HX: 30 points
**
** Please read doublet.dats and
** then try to implement doublet2
**
*)

(* ****** ****** *)
//
// HX-2016-06-21:
// Please implement
// the function doublet2 here!
//
(* ****** ****** *)

staload "libc/SATS/stdlib.sats"

(* ****** ****** *)

implement
main0(argc, argv) = let
//
val w1 =
(
  if argc >= 2 then argv[1] else ""
) : string // end-of-val
val w2 =
(
  if argc >= 3 then argv[2] else ""
) : string // end-of-val
//
val opt = doublet2(w1, w2)
//
in
//
case+ opt of
| None() =>
  println!(w1, '/', w2, " are not a doublet!")
| Some(nxs) =>
  println!(w1, '/', w2, " are a doublet: ", nxs)
//
end // end of [main0]

(* ****** ****** *)

(* end of [assign5-2_sol.dats] *)
