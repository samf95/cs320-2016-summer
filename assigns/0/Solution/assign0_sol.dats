(* ****** ****** *)

#include "./../assign0.dats"

(* ****** ****** *)
//
// HX:
// Please replace
// this dummy implementation
//
(*
implement
triangle_test(x, y, z) = true // please FIXIT!
*)
//
implement
triangle_test
  (x, y, z) =
(
  ifcase
    | x + y <= z => false
    | y + z <= x => false
    | z + x <= y => false | _(*else*) => true
)
//
(* ****** ****** *)

(* end of [assign0_sol.dats] *)
