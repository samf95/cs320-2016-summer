(* ****** ****** *)
//
#include
"./../assign3-2.dats"
//
(* ****** ****** *)
//
// HX-2016-05-30: dummy
//
implement
{a}(*tmp*)
mylist0_contain
  (xs, x0, eq) = true
//
(* ****** ****** *)
//
// HX-2016-05-30: dummy
//
implement
{a}(*tmp*)
mylist0_find (xs, pred) = 0

(* ****** ****** *)
//
// HX-2016-05-30: dummy
//
implement
{a}(*tmp*)
mylist0_rfind (xs, pred) = 0
//
(* ****** ****** *)

#ifdef
MAIN_NONE
#then
#else
implement
main0 () =
{
//
val xs = list0_make_intrange (0, 10)
val () = assertloc (mylist0_contain<int> (xs, 5, lam (x1, x2) => x1 = x2))
val () = assertloc (~mylist0_contain<int> (xs, 10, lam (x1, x2) => x1 = x2))
//
val () = assertloc (mylist0_find<int> (xs, lam (x) => x > 4) = 5)
val () = assertloc (mylist0_rfind<int> (xs, lam (x) => x mod 3 = 1) = 7)
//
val () = println! ("Good news: Your code has passed initial testing!")
//
} (* end of [main0] *)
#endif // #ifdef

(* ****** ****** *)

(* end of [assign3-2_sol.dats] *)
