//
// Title:
// Concepts of Programming Languages
// Number: CAS CS 320
// Semester: Summer I, 2016
// Class Time: MTWR 3:00-5:00
// Instructor: Hongwei Xi (hwxiATbuDOTedu)
// Teaching Fellow: Hanwen Wu (hwwuATbuDOTedu)
//
(*
//
// Assignment #1
// Due Friday, the 27th of May, 2016 at 11:59pm
//
*)
(* ****** ****** *)
//
#include "share/atspre_staload.hats"
//
(* ****** ****** *)
//
extern
fun fib(n: int): int
//
implement
fib(n) =
if n >= 2
  then fib(n-1)+fib(n-2) else n
// end of [if]
//
(* ****** ****** *)
//
(*
** 10 points
** Please give a tail-recursive
** implementation of fib_trec
*)
extern
fun fib_trec(n: int): int // fib_trec(n) = fib(n)
//
(* ****** ****** *)
//
extern
fun
fact(n: int): int
//
implement
fact(n) = if n > 0 then n * fact(n-1) else 1
//
(* ****** ****** *)
//
// HX: 10 points
// Please implement a function [try_fact] that
// finds the first [n] such that fact(n) equals 0:
//
extern
fun try_fact((*void*)): int
//
(* ****** ****** *)

(* end of [assign1.dats] *)
